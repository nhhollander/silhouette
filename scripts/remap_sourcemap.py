#!/usr/bin/python3

# This file takes a generated source map and links all files into a
# subdirectory relative to the map file. This allows the files to be accessed
# directly through the web server.

import sys
import json
import os
import re
import shutil

if len(sys.argv) != 2:
    print(f"usage: ${sys.argv[0]} map_file")
    exit(-1)

mapfile = sys.argv[1]
try:
    mapdata = json.load(open(mapfile, "r"))
except FileNotFoundError:
    print("\033[31mError:\033[0m Map file does not exist")
    exit(-1)
except PermissionError:
    print("\033[31mError:\033[0m Access Denied")
    exit(-1)

if 'se_remapped' in mapdata:
    print(f"Skipping already remapped file '{mapfile}'")
    exit(0)

map_base, map_name = os.path.split(mapfile)

target_dir = f"{map_base}/src"
os.makedirs(target_dir, exist_ok=True)

new_srcs = []
for filename in mapdata['sources']:
    abs_to_file = os.path.abspath(f"{map_base}/{filename}")
    friendly_name = re.sub(r"(?:\.\.\/)*", "", filename)
    abs_to_target = os.path.abspath(f"{target_dir}/{friendly_name}")
    abs_to_target_base = os.path.split(abs_to_target)[0]
    target_to_file = os.path.relpath(abs_to_file, abs_to_target_base)

    # print(f"abs_to_file:        {abs_to_file}")
    # print(f"friendly_name:      {friendly_name}")
    # print(f"abs_to_target:      {abs_to_target}")
    # print(f"abs_to_target_base: {abs_to_target_base}")
    # print(f"target_to_file:     {target_to_file}")
    # print()

    os.makedirs(abs_to_target_base, exist_ok=True)
    
    try:
        os.unlink(abs_to_target, )
    except FileNotFoundError: pass

    os.symlink(target_to_file, abs_to_target)

    new_srcs.append(friendly_name)

mapdata['sources'] = new_srcs
mapdata['se_remapped'] = True # Prevents remapping remapped files

outfile = f"{target_dir}/{map_name}"
json.dump(mapdata,(open(outfile, "w")))

print(f"Remapped \033[32m{mapfile}\033[0m -> \033[32m{outfile}\033[0m")
