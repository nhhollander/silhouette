#!/bin/bash

# This doesn't actually need to be its own file, but because of the way that
# makefiles work, if I just call the `ln` command directly, it'll generate an
# error or a warning and that just isn't acceptable.

ln -s ../../../data build/examples/bin 2> /dev/null

exit 0