#!/bin/bash

# This script counts the lines of code in the project. This functionality used
# to be directly included in the Makefile, but the number of files and places to
# look was getting a bit out of hand so I broke it out into this script.

if ! command -v cloc &> /dev/null; then
    printf "\033[32mError: `cloc` command not found, unable to count lines.\033[0m\n"
    exit 1
fi

# Add data directories to the search list
SEARCH="./include ./src ./examples ./data ./scripts ./docs/pages"
# Add specific files to the search list
SEARCH="$SEARCH CMakeLists.txt README.md"

cloc $SEARCH