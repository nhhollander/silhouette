#!/bin/bash

# This script generates and post-processes documentation for silhouette.

printf "\033[33mGenerating documentation...\033[0m\n"

cmdcheck=0

function check_command {
    command -v $1 &> /dev/null
    return $?
    if ! command -v $1 &> /dev/null; then
        printf "\033[33mWarning: Command '$1' required for generating documentation not found\033[0m\n"
        cmdcheck=1
    fi
}

check_command doxygen
check_command dot

# Run the figure generator
$(dirname "$0")/export_figures.sh

if [ "$cmdcheck" -eq "1" ]; then
    printf "\033[31mSkipping documentation generation due to missing dependencies\033[0m\n"
    exit 0
fi

echo Running doxygen
cd docs
doxygen && touch ./
