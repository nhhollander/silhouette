#!/bin/bash

# This script generates a symlink from the local computer's web server directory
# to the wasm build directory. Depending on your distro or server options, you
# might need to run this script as root.

if [ -d "/var/www/html" ]; then
    WEBDIR="/var/www/html"
elif [ -d "/srv/www/html" ]; then
    WEBDIR="/srv/www/html"
else
    printf "\033[31mCould not find your web file directory :(\033[0m\n"
    printf "Either modify create_wasm_link.sh to point to your web install location\n"
    printf "Or manually create the symlink.\n"
    exit 1
fi

if [ ! -d "build/examples/test/"]; then
    printf "\033[31mWASM not built yet!\033[0m\n"
    printf "The wasm site hasn't been built yet, so the script can't make the link\n"
    exit 2
fi

cd build/examples/test
ln -s $(pwd) $WEBDIR/silhouette