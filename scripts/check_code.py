#!/usr/bin/python3

# Check the code using `cppcheck` and print details about the result

import subprocess
import re
import os

if not os.path.exists("build/compile_commands.json"):
    print("Error: compile_commands.json not found.")
    print("Rebuild and try again")
    exit(1)

template = "«{severity}» {file}:{line}:{column}: {id}: {message}\\n{code}\\n\\n"

try:
    os.unlink("build/cppcheck_report.txt")
except FileNotFoundError: pass

pwd = os.getcwd()

CHECK_CONFIG=False
SUPPRESS_MISSING_INCLUDE=True

command = f"""
    cppcheck \\
        --enable=all \\
        --suppress=unusedFunction \\
        {"--suppress=missingIncludeSystem" if SUPPRESS_MISSING_INCLUDE else ""} \\
        -I {pwd}/include/ \\
        --quiet \\
        --template="{template}" \\
        --inline-suppr \\
        --force \\
        src/ \\
        examples/ \\
        {"--check-config" if CHECK_CONFIG else ""} \\
    2> build/cppcheck_report.txt
"""
#print(f"Command is {command}")
print("\033[33mRunning cppcheck...\033[0m")
subprocess.run(command, shell=True)

# subprocess.run(f"""
#     cppcheck \\
#     --enable=all \\
#     -I include \\
#     -I deps/fmt/include \\
#     -I deps/glm \\
#     -I deps/entt/src \\
#     -I deps/emsdk/upstream/emscripten/system/include/ \\
#     -I deps/emsdk/upstream/emscripten/system/include/SDL \\
#     --template="{template}" \\
#     --inline-suppr \\
#     -U ANDROID \\
#     -U WIN32 \\
#     -U _WIN32 \\
#     -U FMT_HEADER_ONLY \\
#     -U FMT_DOC \\
#     -U MINGW32 \\
#     -U CYGWIN \\
#     -U __MINGW32__ \\
#     -U __CYGWIN__ \\
#     src/ \\
#     2> build/cppcheck_report.txt
# """, shell=True)

report = open("build/cppcheck_report.txt","r").read()

if len(report) > 1:
    # If the file is empty (or just a newline) don't bother printing it
    print(report)

severities = {
    "error": 0,
    "warning": 0,
    "style": 0,
    "performance": 0,
    "portability": 0,
    "information": 0
}
for match in re.finditer(r"«(.*?)»", report):
    severity = match.group(1)
    if not severity in severities:
        print(f"\033[33mWARN:\033[0m Issue with unknown severity '{severity}'")
        continue
    severities[severity] += 1


print(f"\033[31mError\033[0m {severities['error']}  \033[33mWarn\033[0m {severities['warning']}  \033[36mStyle\033[0m {severities['style']}  \033[32mPerf\033[0m {severities['performance']}  \033[35mPort\033[0m {severities['portability']}  \033[34mInfo\033[0m {severities['information']}")
print(f"\033[1mTotal:\033[0m {sum(severities.values())}")
