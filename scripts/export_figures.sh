#!/bin/bash

# This script automatically exports all of the diagrams in this folder as SVGs
# and places them in the `docs/img` folder.

echo "Updating figures"

# Check if drawio is installed
if ! command -v drawio &> /dev/null
then
    printf "\033[1;31mError:\033[0m "
    printf "drawio is not installed on your system!\n"
    printf "It can be downloaded from https://draw.io/\n"
    exit -1
fi

# Export the figures
cd docs/drawio
mkdir -p ../img
for file in *.drawio; do
    out="../img/$file.svg"
    if [ "$file" -nt "$out" ]; then
        drawio --export --output ../img --format svg --transparent --border 5 "$file"
    fi
done
