#!/bin/bash

# Workspace initialization

printf "\033[33mResolving dependencies...\033[0m\n"
git submodule update --init --recursive

printf "\033[33mConfiguring emscripten...\033[0m\n"
(
    cd deps/emsdk
    printf "\033[1mInstalling latest sdk...\033[0m\n"
    ./emsdk install latest
    printf "\033[1mActivating latest sdk...\033[0m\n"
    ./emsdk activate latest
    printf "\033[33mYou can ignore the 'Next Steps' message above\033[0m\n"
)

touch .repo_initialized
