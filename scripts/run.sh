#!/bin/bash

# Check for web server
wget localhost -q -O /dev/null
if [ $? -eq 0 ]; then
    printf "\033[31mIt looks like you already have a web server running on this computer!\033[0m\n"
    printf "\033[31mYou should symlink it to the wasm output directory instead of using this script.\033[0m\n"
    printf "\033[31mTry running ./scripts/create_wasm_link.sh\033[0m\n"
    exit 1
fi

cd build/wasm/examples
python3 -m http.server &
PY_PID=$!
firefox http://localhost:8000/test.html

printf "\033[33mPress enter to terminate server...\033[0m\n"
read
kill $PY_PID
printf "\033[33mServer terminated.\033[0m\n"