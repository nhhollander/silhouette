#!/usr/bin/python3

import cgi

args = cgi.parse()

if not "code" in args:
    err = 500
else:
    err = args["code"][0]

if not "msg" in args:
    msg = "Debug Response"
else:
    msg = args["msg"][0]

print(f"Status: {err} {msg}")
print("Content-Type: text/html")
print("\r\n\r\n")
print("<h1>This is a test response</h1>")
print(f"Response code: <pre>{err}</pre>")
