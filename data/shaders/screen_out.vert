#version 300 es
/**
 * Direct-to-screen vertex shader.
 * 
 * Copyright 2021 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

precision mediump float;

in vec3 pos;
in vec2 uv;

out vec2 uv_frag;

void main() {
    gl_Position = vec4(pos.x, pos.y, 0.0, 1.0);
    uv_frag = uv;
}
