#version 300 es
/**
 * Direct-to-screen fragment shader.
 * 
 * Copyright 2021 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

precision mediump float;

in vec2 uv_frag;

out vec4 color;

void main() {
    color = vec4(uv_frag.x, uv_frag.y, 0.0, 1.0);
}
