#version 300 es

/* This is the default vertex shader. It is special, in that it is always
 * loaded, and will be used as a placeholder whenever the engine has not yet
 * finished loading, or has failed to load, a different shader program. */

precision mediump float;

in vec2 pos;

void main() {
    gl_Position = vec4(pos.x, pos.y, 0.0, 1.0);
}