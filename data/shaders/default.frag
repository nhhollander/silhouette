#version 300 es

/* This is the default fragment shader. It is special, in that it is always
 * loaded, and will be used as a placeholder whenever the engine has not yet
 * finished loading, or has failed to load, a different shader program. */

precision mediump float;

out vec4 color;

void main() {
    color = vec4(1.0, 0.0, 1.0, 1.0);
}