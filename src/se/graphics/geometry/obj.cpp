#include "se/graphics/geometry/obj.hpp"

#include "se/graphics/geometry/loader.hpp"
#include "se/util/log.hpp"

#include <glm/vec3.hpp>
#include <glm/vec2.hpp>

#define OBJ_LINE_HEADER_MAX 128

#define SS(X) #X   // Cursed c/c++ preprocessor hack
#define S(X) SS(X) // Cursed c/c++ preprocessor hack

#define header(X) strcmp(line_header, X) == 0

using namespace se::graphics::geometry;

/* This code is adapted from the amazing model loading tutorial at
http://www.opengl-tutorial.org/beginners-tutorials/tutorial-7-model-loading/ */
bool se::graphics::geometry::load_obj(std::string name, std::vector<char> data, Geometry* geom) {
    DEBUG("[{}] Decoding OBJ", name);

    // Null terminate data vector to prevent UB if the file ends unexpectedly
    data.push_back('\0');

    std::vector<unsigned int> vertex_indices;
    std::vector<unsigned int> uv_indices;
    std::vector<unsigned int> normal_indices;

    std::vector<glm::vec3> tmp_vertices;
    std::vector<glm::vec2> tmp_uvs;
    std::vector<glm::vec3> tmp_normals;

    char line_header[OBJ_LINE_HEADER_MAX+1];

    char* data_ptr = data.data();
    int bytes_read = 0;

    int dbg_ctr = 0;

    while(true) {
        // Read the line header
        //INFO("Parsing {}/{} bytes", data_ptr - data.data(), data.size());
        int res = sscanf(data_ptr, "%" S(OBJ_LINE_HEADER_MAX) "s%n", line_header, &bytes_read);
        data_ptr += bytes_read;
        if(res == EOF) break;
        if(dbg_ctr++ > 1500) break;

        bytes_read = 0;

        if(header("v")) {
            glm::vec3 vertex;
            sscanf(data_ptr, "%f %f %f\n%n", &vertex.x, &vertex.y, &vertex.z, &bytes_read);
            data_ptr += bytes_read;
            tmp_vertices.push_back(vertex);
        }
        else if(header("vt")) {
            glm::vec2 uv;
            sscanf(data_ptr, "%f %f\n%n", &uv.x, &uv.y, &bytes_read);
            data_ptr += bytes_read;
            tmp_uvs.push_back(uv);
        }
        else if(header("vn")) {
            glm::vec3 normal;
            sscanf(data_ptr, "%f %f %f\n%n", &normal.x, &normal.y, &normal.z, &bytes_read);
            data_ptr += bytes_read;
            tmp_normals.push_back(normal);
        }
        else if(header("f")) {
            unsigned int vert_index[3];
            unsigned int uv_index[3];
            unsigned int normal_index[3];
            int matches = sscanf(data_ptr, "%u/%u/%u %u/%u/%u %u/%u/%u\n%n",
                &vert_index[0],   &vert_index[1],   &vert_index[2],
                &uv_index[0],     &uv_index[1],     &uv_index[2],
                &normal_index[0], &normal_index[1], &normal_index[2],
                &bytes_read);
            data_ptr += bytes_read;
            if(matches != 9) {
                WARN("[{}] Unable to parse file! Try exporting with other options. "
                    "(Incorrect number of indices in fragment definition)", name);
                return false;
            }
            vertex_indices.push_back(vert_index[0]);
            vertex_indices.push_back(vert_index[1]);
            vertex_indices.push_back(vert_index[2]);
            uv_indices    .push_back(uv_index[0]);
            uv_indices    .push_back(uv_index[1]);
            uv_indices    .push_back(uv_index[2]);
            normal_indices.push_back(normal_index[0]);
            normal_indices.push_back(normal_index[1]);
            normal_indices.push_back(normal_index[2]);
        }

        //DEBUG("{} bytes read from section", bytes_read);
    }

    if(vertex_indices.size() != uv_indices.size() || uv_indices.size() != normal_indices.size()) {
        ERROR("[{}] Unable to process model, Unequal index counts! "
            "V: {} U: {} N: {}",
            geom->get_extended_name(),
            vertex_indices.size(), uv_indices.size(), normal_indices.size());
        return false;
    }

    std::vector<Vertex> vertices;

    for(unsigned int i = 0; i < vertex_indices.size(); i++) {
        Vertex v;
        v.position = tmp_vertices[vertex_indices[i] - 1];
        v.uv = tmp_uvs[uv_indices[i] - 1];
        v.normal = tmp_normals[normal_indices[i] - 1];
        vertices.push_back(v);
    }

    geom->set_geometry(vertices);
    return true;
}
