#include "se/graphics/geometry/loader.hpp"

#include "se/graphics/geometry/obj.hpp"
#include "se/util/log.hpp"

using namespace se::graphics::geometry;
using namespace se::util;

bool se::graphics::geometry::decode(std::string name, std::vector<char> data, Geometry* geom) {
    
    // OBJ files can only be detected by extension
    if(name.ends_with(".obj") || name.ends_with(".OBJ")) {
        return load_obj(name, data, geom);
    }
    else {
        ERROR("[{}] Model file type could not be recognized!", name);
        geom->set_state(resource::ResourceState::ERROR);
        return false;
    }
}