#include "se/graphics/shader.hpp"

#include "se/engine.hpp"
#include "se/graphics/graphics.hpp"
#include "se/util/fileio.hpp"
#include "se/util/hash.hpp"
#include "se/util/log.hpp"
#include "se/util/opengl.hpp"

#include <chrono>
#include <map>
#include <SDL.h>
#include <GL/glew.h>
#include <SDL_opengl.h>
#include <vector>

using namespace se::util;

const char* shadertype(unsigned int type) {
    switch(type) {
        case(GL_FRAGMENT_SHADER): return "FRAG";
        case(GL_VERTEX_SHADER): return "VERT";
        default: return "<Invalid Shader Type>";
    }
}

void se::graphics::shader::Shader::load_() {
    DEBUG("[{}] Loading", this->get_extended_name());
    this->set_state(resource::ResourceState::LOADING);

    /* Note: Because shaders can not be unloaded (for simplicity) there is
     * no need to implement separate unload checks. */

    auto loaded = [this](const std::string& target, const std::vector<char>& raw, fileio::Result result, void* arg) {
        if(this->get_resource_state() != resource::ResourceState::LOADING) {
            WARN("[{}] Resource not in LOADING state! ({})", this->get_extended_name(),
                resource_state_name(this->get_resource_state()));
            return;
        }
        if(result == fileio::Result::FAILURE) {
            ERROR("[{}] Shader source load failure!", this->get_extended_name());
            this->set_state(resource::ResourceState::ERROR);
            return;
        }
        this->src = raw;
        /* Insert a null terminator at the end of the string, so that when it is
         * passed to the compiler as a raw string, the risk of a memory overrun
         * is elminated. */
        this->src.push_back('\0');
        se::engine::register_job([this](){this->compile();});
    };

    std::string fname = "shaders/";
    fname += this->get_name();
    if(type == GL_VERTEX_SHADER) {
        fname += ".vert";
    } else if(type == GL_FRAGMENT_SHADER) {
        fname += ".frag";
    } else {
        ERROR("[{}:{}] Invalid shader type",
            this->get_extended_name(), shadertype(this->type));
        this->set_state(resource::ResourceState::ERROR);
        return;
    }

    DEBUG("[{}] Reading from [{}]", this->get_extended_name(), fname);

    fileio::read(fname, loaded);
}

void se::graphics::shader::Shader::unload_() {
    // This resource can not be manually unloaded
}

void se::graphics::shader::Shader::compile() {
    DEBUG("[{}] Compiling", this->get_extended_name());

    opengl::clear();

    this->id = glCreateShader(this->type);
    const GLchar* shader_sources[] = {
        this->src.data()
    };
    glShaderSource(this->id, 1, shader_sources, nullptr);
    glCompileShader(this->id);

    opengl::check_error();

    GLint shader_compiled = GL_FALSE;
    glGetShaderiv(this->id, GL_COMPILE_STATUS, &shader_compiled);
    if(shader_compiled != GL_TRUE) {
        // Get the shader error log
        int max_length = 0;
        glGetShaderiv(this->id, GL_INFO_LOG_LENGTH, &max_length);
        char* log = new char[max_length];
        int actual_length = 0;
        glGetShaderInfoLog(this->id, max_length, &actual_length, log);
        if(actual_length > 0) {
            ERROR("Failed to compile shader [{}]:\n{}",
                this->get_extended_name().c_str(), log);
            ERROR("Shader Source:\n{}", this->src.data());
        } else {
            ERROR("Failed to compile shader [{}]: No log available",
                this->get_extended_name().c_str());
        }
        this->set_state(resource::ResourceState::ERROR);
        delete[] log;
        return;
    }
    this->set_state(resource::ResourceState::LOADED);
}

std::string se::graphics::shader::Shader::calculate_extended_name(std::string name, int type) {
    return fmt::format("{}:{}", name, shadertype(type));
}

void se::graphics::shader::Shader::__configure(int type) {
    this->type = type;
}
