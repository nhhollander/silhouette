#include "se/graphics/program.hpp"

#include "se/engine.hpp"
#include "se/graphics/graphics.hpp"
#include "se/util/hash.hpp"
#include "se/util/log.hpp"
#include "se/util/opengl.hpp"

#include <SDL.h>
#include <GL/glew.h>
#include <SDL_opengl.h>
#include <thread>
#include <chrono>
#include <map>

using namespace se::util;

void se::graphics::program::Program::load_() {
    this->set_state(resource::ResourceState::LOADING);
    this->frag->increment_resource_user_count();
    this->vert->increment_resource_user_count();

    auto shader_cb = [this](Resource* res){
        // Determine caller information
        std::string child_name;
        if(res == this->frag) {
            child_name = this->frag->get_extended_name();
        } else if(res == this->vert) {
            child_name = this->vert->get_extended_name();
        } else {
            WARN("[{}] Got callback with wrong child!", this->get_extended_name());
            child_name = "<Wrong Child>";
        }
        // Check states
        if(this->get_resource_state() == resource::ResourceState::ERROR) {
            WARN("[{}] Ignoring shader callback due to error state",
                this->get_extended_name());
            return;
        }
        if(res->get_resource_state() == resource::ResourceState::ERROR) {
            ERROR("[{}] Required child resource [{}] entered error state!",
                this->get_extended_name(), child_name);
            this->set_state(resource::ResourceState::ERROR);
        }
        if(this->frag->get_resource_state() == resource::ResourceState::LOADED &&
            this->vert->get_resource_state() == resource::ResourceState::LOADED) {
            // Everything in correct state, Launch the linker
            se::engine::register_job([this](){this->link();});
        }

    };
    // Check resource states, and register delayed link if necessary
    if(this->frag->get_resource_state() == resource::ResourceState::ERROR ||
        this->vert->get_resource_state() == resource::ResourceState::ERROR) {
        ERROR("[{}] Unable to load program, one or more children in error state! [{}: {}] [{}: {}]",
            this->get_extended_name(),
            this->vert->get_extended_name(), resource_state_name(this->vert->get_resource_state()),
            this->frag->get_extended_name(), resource_state_name(this->frag->get_resource_state()));
        return;
    }
    if(this->frag->get_resource_state() != resource::ResourceState::LOADED) {
        this->frag->add_state_change_listener(shader_cb, resource::ResourceState::LOADED | resource::ResourceState::ERROR);
    }
    if(this->vert->get_resource_state() != resource::ResourceState::LOADED) {
        this->vert->add_state_change_listener(shader_cb, resource::ResourceState::LOADED | resource::ResourceState::ERROR);
    }
    if(this->frag->get_resource_state() == resource::ResourceState::LOADED &&
        this->vert->get_resource_state() == resource::ResourceState::LOADED) {
        // Everything in correct state, launch linker
        se::engine::register_job([this](){this->link();});
    }
}

void se::graphics::program::Program::unload_() {
    ERROR("[{}] Programs can not be unloaded", this->get_extended_name());
}

void se::graphics::program::Program::link() {
    // Check if shaders are errored
    if(this->frag->get_resource_state() == resource::ResourceState::ERROR) {
        ERROR("[{}] Unable to link, fragment shader [{}] failed to load.",
            this->get_extended_name(), this->frag->get_extended_name());
        this->set_state(resource::ResourceState::ERROR);
        return;
    }
    if(this->vert->get_resource_state() == resource::ResourceState::ERROR) {
        ERROR("[{}] Unable to link, vertex shader [{}] failed to load.",
            this->get_extended_name(), this->vert->get_extended_name());
        this->set_state(resource::ResourceState::ERROR);
        return;
    }
    // Final sanity check
    if(this->frag->get_resource_state() != resource::ResourceState::LOADED ||
        this->vert->get_resource_state() != resource::ResourceState::LOADED) {
        ERROR("[{}] Error, shaders in invalid state [F:{}] [V:{}] (You should never see this message)",
            this->get_extended_name(),
            resource_state_name(this->frag->get_resource_state()),
            resource_state_name(this->vert->get_resource_state()));
        this->set_state(resource::ResourceState::ERROR);
        return;
    }

    opengl::clear();

    // Sanity check passed - begin linking process
    this->id = glCreateProgram();
    glAttachShader(this->id, this->frag->get_id());
    glAttachShader(this->id, this->vert->get_id());
    glLinkProgram(this->id);

    opengl::check_error();

    GLint program_linked = GL_FALSE;
    glGetProgramiv(this->id, GL_LINK_STATUS, &program_linked);
    if(program_linked != GL_TRUE) {
        // Link failure - Get the linker error log
        int max_length = 0;
        glGetProgramiv(this->id, GL_INFO_LOG_LENGTH, &max_length);
        char* log = new char[max_length];
        int actual_length = 0;
        glGetShaderInfoLog(this->id, max_length, &actual_length, log);
        if(actual_length > 0) {
            ERROR("[{}] Failed to link program:\n{}",
                this->get_extended_name(), log);
        } else {
            ERROR("[{}] Failed to link program: No log available",
                this->get_extended_name());
        }
        this->set_state(resource::ResourceState::ERROR);
        delete[] log;
        return;
    }
    this->set_state(resource::ResourceState::LOADED);
}

bool se::graphics::program::Program::use() {

    // Ensure loaded
    if(this->get_resource_state() != resource::ResourceState::LOADED) {
        if(this->get_resource_state() == resource::ResourceState::ERROR) {
            WARN("[{}] Attempted to use program in error or child error state.",
                this->get_extended_name());
        } else {
            WARN("[{}] Attempted to use program before it was linked. [{}]",
                this->get_extended_name(),
                resource_state_name(this->get_resource_state()));
        }
        if(default_program != nullptr) {
            if(default_program != this) {
                if(default_program->get_resource_state() == resource::ResourceState::LOADED) {
                    default_program->use();
                    return true;
                }
            }
        }
        return false;
    }

    glUseProgram(this->id);
    
    return true;
}

void se::graphics::program::Program::__configure() {

    DEBUG("[{}] Initializing shaders", this->get_extended_name());
    this->vert = resource::get<graphics::shader::Shader>(this->get_name(), GL_VERTEX_SHADER);
    this->frag = resource::get<graphics::shader::Shader>(this->get_name(), GL_FRAGMENT_SHADER);

    if(default_program == nullptr) {
        default_program = this;
    }

}

se::graphics::program::Program* se::graphics::program::default_program = nullptr;
