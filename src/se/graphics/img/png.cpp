#include "se/graphics/img/png.hpp"

#include "se/util/log.hpp"
#include "se/util/time.hpp"

#include <png.h>
#include <memory.h>
#include <emscripten.h>

#define PNG_SIG_LEN 8

using namespace se::graphics::texture;
using namespace se::util;

bool se::graphics::img::is_png(Texture* texture) {

    if(texture->raw_encoded.size() < 8) {
        // Data is too short to be a PNG
        return false;
    }

    unsigned char sig[PNG_SIG_LEN];
    memcpy(sig, texture->raw_encoded.data(), PNG_SIG_LEN);
    if(png_sig_cmp((const unsigned char*) sig, 0, PNG_SIG_LEN) != 0) {
        // Not a PNG file
        return false;
    }

    return true;
}

bool se::graphics::img::load_png(Texture* texture) {

    DEBUG("Reading PNG [{}]", texture->get_extended_name());

    auto abort = [texture]() {
        texture->set_state(resource::ResourceState::ERROR);
        texture->raw_encoded.clear();
        texture->width = 0;
        texture->height = 0;
        texture->pixel_format = UNDEFINED;
    };

    if(texture->virt) {
        ERROR("[{}s] Cannot load PNG into a virtual texture!",
            texture->get_extended_name());
        abort();
        return false;
    }

    if(!is_png(texture)) {
        ERROR("[{}] Not a PNG file!", texture->get_extended_name());
        abort();
        return false;
    }

    png_structp png_ptr =
        png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if(!png_ptr) {
        ERROR("[{}] png_create_read_struct failed!", texture->get_extended_name());
        abort();
        return false;
    }

    png_infop info_ptr = png_create_info_struct(png_ptr);
    if(!info_ptr) {
        ERROR("[{}] png_create_info_struct failed!", texture->get_extended_name());
        abort();
        png_destroy_read_struct(&png_ptr, nullptr, nullptr);
        return false;
    }

    /* Because File IO is handled by the FileIO system, libpng isn't going to
     * get a file handle like it expects, instead we have to implement a custom
     * read routine. */
    struct png_read_fn_info {
        size_t png_read_ctr;
        Texture* texture;
    };
    png_read_fn_info info {0, texture};
    auto read_function = [](png_structp png_ptr, png_bytep out, png_size_t count) {
        png_read_fn_info* info = static_cast<png_read_fn_info*>(png_get_io_ptr(png_ptr));
        if(info == nullptr) {
            ERROR("PNG read function got null pointer! No other info.");
            return;
        }
        size_t read_count = count;
        if(info->png_read_ctr + read_count > info->texture->raw_encoded.size()) {
            read_count = info->texture->raw_encoded.size() - info->png_read_ctr;
        }
        memcpy(out, &info->texture->raw_encoded.data()[info->png_read_ctr], read_count);
        info->png_read_ctr += read_count;
    };
    png_set_read_fn(png_ptr, &info, read_function);

    // Get image info
    png_read_info(png_ptr, info_ptr);

    png_uint_32 width = 0;
    png_uint_32 height = 0;
    int bit_depth = 0;
    int color_type = -1;
    png_uint_32 res = png_get_IHDR(png_ptr, info_ptr, &width, &height,
        &bit_depth, &color_type, NULL, NULL, NULL);

    if(res != 1) {
        ERROR("[{}] Failed to read image info!", texture->get_extended_name());
        abort();
        png_destroy_read_struct(&png_ptr, nullptr, nullptr);
        return false;
    }

    texture->width = width;
    texture->height = height;

    /* 
     * Adapted from the libPNG docs:
     * | Type | Depths     | Interpretation                                    |
     * |======|============|===================================================|
     * | 0    | 1,2,4,8,16 | Each pixel is grayscale.                          |
     * | 2    | 8,16       | Each pixel is in an RGB triple.                   |
     * | 3    | 1,2,4,8    | Each pixel is palette index, PLTE chunk reqd.     |
     * | 4    | 8,16       | Each pixel is grayscale plus alpha.               |
     * | 6    | 8,16       | Each pixel is in an RGB triple plus alpha.        |
     * 
     * Silhouette uses the following mappings to Texture
     * | Type | Depth | SE PixelFormat |
     * |======|=======|================|
     * | 0    | 1     |    unsupported |
     * | 0    | 2     |    unsupported |
     * | 0    | 4     |    unsupported |
     * | 0    | 8     | GRAY8          |
     * | 0    | 16    | GRAY16         |
     * | 2    | 8     | RGB8           |
     * | 2    | 16    | RGB16          |
     * | 3    | 1     |    unsupported |
     * | 3    | 2     |    unsupported |
     * | 3    | 4     |    unsupported |
     * | 3    | 8     |    unsupported |
     * | 6    | 8     | RGBA8          |
     * | 6    | 16    | RGBA16         |
     */

    #define TYPEMAP(type,depth,fmt) \
        if(color_type == type && bit_depth == depth) { \
            texture->pixel_format = fmt; \
        }

    TYPEMAP(0, 8,  GRAY8)  else
    TYPEMAP(0, 16, GRAY16) else
    TYPEMAP(2, 8,  RGB8)   else
    TYPEMAP(2, 16, RGB16)  else
    TYPEMAP(6, 8,  RGBA8)  else
    TYPEMAP(6, 16, RGBA16)
    else {
        ERROR("[{}] Unrecognized or unsupported color type and bit depth! "
            "[Type: {} Depth: {}]",
            texture->get_extended_name(), color_type, bit_depth);
        abort();
        png_destroy_read_struct(&png_ptr, nullptr, nullptr);
        return false;
    }

    // New image reading system
    {
        size_t row_bytes = png_get_rowbytes(png_ptr, info_ptr);
        size_t raw_length = height * row_bytes;
        texture->raw.resize(raw_length);

        png_bytep* row_pointers = new png_bytep[height];
        for(png_uint_32 y = 0; y < height; y++) {
            row_pointers[y] = texture->raw.data() + (y * row_bytes);
        }
        //png_read_image(png_ptr, row_pointers);

        }

    // Read the image data
    {
    size_t row_bytes = png_get_rowbytes(png_ptr, info_ptr);
    png_bytep* row_pointers = new png_bytep[height];
    for(png_uint_32 y = 0; y < height; y++) {
        row_pointers[y] = new png_byte[row_bytes];
    }
    png_read_image(png_ptr, row_pointers);

    for(png_uint_32 y = 0; y < height; y++) {
        memcpy(&(texture->raw.data()[y * row_bytes]), row_pointers[y], row_bytes);
    }
    for(png_uint_32 y = 0; y < height; y++) {
        delete[] row_pointers[y];
    }
    delete[] row_pointers;

    }


    texture->raw_encoded.clear();
    // Done!
    return true;
}