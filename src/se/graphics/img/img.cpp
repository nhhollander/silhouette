#include "se/graphics/img/img.hpp"

#include "se/util/log.hpp"

using namespace se::graphics::texture;

bool se::graphics::img::is_image(Texture* texture) {
    
    if(is_png(texture)) return true;
    // Add more supported image types here

    return false;

}

bool se::graphics::img::load_image(Texture* texture) {

    if(is_png(texture))
        return load_png(texture);
    // Add more supported image types here

    ERROR("[{}] Unable to load. Image type not recognized.",
        texture->get_extended_name());
    return false;

}