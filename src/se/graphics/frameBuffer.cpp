#include "se/graphics/frameBuffer.hpp"

#include "se/graphics/graphics.hpp"
#include "se/util/hash.hpp"
#include "se/util/log.hpp"
#include "se/util/opengl.hpp"

#include <GL/glew.h>

#include <map>
#include <mutex>
#include <SDL.h>
#include <SDL_opengl.h>

using namespace se::util;
using namespace se::graphics;
using namespace se::graphics::texture;
using namespace se::util::resource;

#define CASE(X) case X: return #X

// cSpell:ignoreRegExp (?:GRAY[A-Z]?|RGB[A-Z]?)

const char* se::graphics::framebuffer::layerflag_to_str(LayerFlag flag) {
    switch(flag) {
        CASE(ALBEDO);
        CASE(POSITION);
        CASE(NORMAL);
        CASE(SPECULAR);
        CASE(ALPHA);
        CASE(DEPTH);
        default: return "<invalid LayerFlag>";
    }
}

const char* se::graphics::framebuffer::enabledlayers_to_str(EnabledLayers layers) {
    switch(layers) {
        CASE(COLOR);
        CASE(COLORA);
        CASE(COLORD);
        CASE(COLORAD);
        default: return "<invalid EnabledLayers>";
    }
}

std::string se::graphics::framebuffer::FrameBuffer::calculate_extended_name(std::string name, EnabledLayers layers, int default_width, int default_height) {
    // Default width and height are not part of the extended name
    return fmt::format("{}:{}", name, enabledlayers_to_str(layers));
}

void se::graphics::framebuffer::FrameBuffer::__configure(EnabledLayers layers, int default_width, int default_height) {
    if((layers & COLOR) != COLOR) {
        ERROR("[{}] Color flag not set! (Invlaid!)", this->get_extended_name());
        this->set_state(ResourceState::ERROR);
        return;
    }

    this->textures.albedo = get<Texture>(
        fmt::format("{}_albedo", this->get_extended_name()),
        default_width, default_height,
        RGB8
    );
    this->textures.position = get<Texture>(
        fmt::format("{}_position", this->get_extended_name()),
        default_width, default_height,
        RGBAF
        /* WebGL 2.0 with EXT_color_buffer_float allows rendering to basically
         * every 32F image format *except* for RGB32F, I have absolutely no idea
         * why, and nobody online seems to know either. The only practical
         * implication of this is that FrameBuffer's will now use about 8MiB
         * more graphics RAM. Maybe this code could be re-written at some point
         * to use that additional channel for alpha. */
    );
    this->textures.normal = get<Texture>(
        fmt::format("{}_normal", this->get_extended_name()),
        default_width, default_height,
        RGB8
    );
    this->textures.specular = get<Texture>(
        fmt::format("{}_spec", this->get_extended_name()),
        default_width, default_height,
        GRAY8
    );

    // `-Wdeprecated-enum-enum-conversion`
    LayerFlag layer_flags = (LayerFlag) layers;

    if(layer_flags & ALPHA) {
        this->textures.alpha = get<Texture>(
            fmt::format("{}_alpha", this->get_extended_name()),
            default_width, default_height,
            GRAY8
        );
    }

    if(layer_flags & DEPTH) {
        this->textures.depth = get<Texture>(
            fmt::format("{}_depth", this->get_extended_name()),
            default_width, default_height,
            PixelFormat::DEPTH
        );
    }
}

bool se::graphics::framebuffer::FrameBuffer::use() {
    if(this->get_resource_state() != ResourceState::LOADED) {
        return false;
    }
    glBindFramebuffer(GL_FRAMEBUFFER, this->id);
    return true;
}

void se::graphics::framebuffer::FrameBuffer::load_() {
    if(this->get_resource_state() != ResourceState::NOT_LOADED) {
        WARN("[{}] FrameBuffer not not loaded [{}]", this->get_extended_name(),
            resource_state_name(this->get_resource_state()));
        return;
    }
    this->set_state(ResourceState::LOADING);

    // Increment resource counters
    this->textures.albedo->increment_resource_user_count();
    this->textures.position->increment_resource_user_count();
    this->textures.normal->increment_resource_user_count();
    this->textures.specular->increment_resource_user_count();
    if(this->textures.alpha != nullptr) {
        this->textures.alpha->increment_resource_user_count();
    }
    if(this->textures.depth != nullptr) {
        this->textures.depth->increment_resource_user_count();
    }

    auto state_listener = [this](Resource* res) {
        if(this->get_resource_state() != ResourceState::LOADING) {
            /* This happens if this resource is unloaded during the loading
             * process, or one of the children enters an error state */
            WARN("[{}] Not in loading state ({}) "
                "(From state callback for [{}])", this->get_extended_name(),
                resource_state_name(this->get_resource_state()),
                res->get_extended_name());
            return;
        }
        if(res->get_resource_state() == ResourceState::ERROR) {
            ERROR("[{}] Child texture in error state ({})",
                this->get_extended_name(), res->get_extended_name());
            this->set_state(ResourceState::ERROR);
        }
        if(this->textures_ready()) {
            DEBUG("[{}] All textures ready, initiating bind", this->get_extended_name());
            this->bind();
        } else {
            DEBUG("[{}] Not ready, waiting {}", this->get_extended_name(),
                this->textures_status());
        }
    };

    if(this->textures.albedo->get_resource_state() != ResourceState::LOADED) {
        this->textures.albedo->add_state_change_listener(state_listener,
            ResourceState::LOADED | ResourceState::ERROR);
    }
    if(this->textures.position->get_resource_state() != ResourceState::LOADED) {
        this->textures.position->add_state_change_listener(state_listener,
            ResourceState::LOADED | ResourceState::ERROR);
    }
    if(this->textures.normal->get_resource_state() != ResourceState::LOADED) {
        this->textures.normal->add_state_change_listener(state_listener,
            ResourceState::LOADED | ResourceState::ERROR);
    }
    if(this->textures.specular->get_resource_state() != ResourceState::LOADED) {
        this->textures.specular->add_state_change_listener(state_listener,
            ResourceState::LOADED | ResourceState::ERROR);
    }
    if(this->textures.alpha != nullptr) {
        if(this->textures.alpha->get_resource_state() != ResourceState::LOADED) {
            this->textures.alpha->add_state_change_listener(state_listener,
                ResourceState::LOADED | ResourceState::ERROR);
        }
    }
    if(this->textures.depth != nullptr) {
        if(this->textures.depth->get_resource_state() != ResourceState::LOADED) {
            this->textures.depth->add_state_change_listener(state_listener,
                ResourceState::LOADED | ResourceState::ERROR);
        }
    }

    if(this->textures_ready()) {
        DEBUG("[{}] START All textures ready, initiating bind", this->get_extended_name());
        this->bind();
    } else {
        DEBUG("[{}] START Not ready, waiting", this->get_extended_name());
    }

}

void se::graphics::framebuffer::FrameBuffer::unload_() {
    if(this->get_resource_state() != ResourceState::LOADED) {
        WARN("[{}] FrameBuffer not loaded [{}]", this->get_extended_name(),
            resource_state_name(this->get_resource_state()));
        return;
    }
    this->set_state(ResourceState::UNLOADING);

    // Decrement resource counters
    this->textures.albedo->decrement_resource_user_count();
    this->textures.position->decrement_resource_user_count();
    this->textures.normal->decrement_resource_user_count();
    this->textures.specular->decrement_resource_user_count();
    if(this->textures.alpha != nullptr) {
        this->textures.alpha->decrement_resource_user_count();
    }
    if(this->textures.depth != nullptr) {
        this->textures.depth->decrement_resource_user_count();
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glDeleteFramebuffers(1, &this->id);
    this->set_state(ResourceState::NOT_LOADED);

}

void se::graphics::framebuffer::FrameBuffer::bind() {

    if(this->get_resource_state() != ResourceState::LOADING) {
        WARN("[{}] FrameBuffer bind requested but state is not loading! [{}]",
            this->get_extended_name(),
            resource_state_name(this->get_resource_state()));
        return;
    }

    // Sanity check
    if(!this->textures_ready()) {
        ERROR("[{}] FrameBuffer bind requested but textures are not ready! "
            "(You should never see this message)", this->get_extended_name());
        this->set_state(ResourceState::ERROR);
        return;
    }

    opengl::clear();

    // Sanith check passed - begin linking process
    glGenFramebuffers(1, &this->id);
    glBindFramebuffer(GL_FRAMEBUFFER, this->id);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, this->textures.albedo->id, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, this->textures.position->id, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, this->textures.normal->id, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, this->textures.specular->id, 0);

    if(this->textures.alpha != nullptr) {
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D, this->textures.alpha->id, 0);
    }
    if(this->textures.depth != nullptr) {
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, this->textures.depth->id, 0);
    }

    // Check for bind error
    opengl::check_error();
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        ERROR("[{}] Framebuffer is not complete!", this->get_extended_name());
        this->set_state(ResourceState::ERROR);
        return;
    }

    this->set_state(ResourceState::LOADED);

}

bool se::graphics::framebuffer::FrameBuffer::textures_ready() {
    if(this->textures.albedo->get_resource_state() != ResourceState::LOADED) {
        return false;
    }
    if(this->textures.position->get_resource_state() != ResourceState::LOADED) {
        return false;
    }
    if(this->textures.normal->get_resource_state() != ResourceState::LOADED) {
        return false;
    }
    if(this->textures.specular->get_resource_state() != ResourceState::LOADED) {
        return false;
    }
    if(this->textures.alpha != nullptr) {
        if(this->textures.alpha->get_resource_state() != ResourceState::LOADED) {
            return false;
        }
    }
    if(this->textures.depth != nullptr) {
        if(this->textures.depth->get_resource_state() != ResourceState::LOADED) {
            return false;
        }
    }
    return true;
}

std::string se::graphics::framebuffer::FrameBuffer::textures_status() {
    const char* albedo = "\303[31mN";
    const char* position = "\033[31mN";
    const char* normal = "\033[31mN";
    const char* specular = "\033[31mN";
    const char* alpha = "\033[0m=";
    const char* depth = "\033[0m=";
    if(this->textures.albedo->get_resource_state() == ResourceState::LOADED) {
        albedo = "\033[32mY";
    }
    if(this->textures.position->get_resource_state() == ResourceState::LOADED) {
        position = "\033[32mY";
    }
    if(this->textures.normal->get_resource_state() == ResourceState::LOADED) {
        normal = "\033[32mY";
    }
    if(this->textures.specular->get_resource_state() == ResourceState::LOADED) {
        specular = "\033[32mY";
    }
    if(this->textures.alpha != nullptr) {
        if(this->textures.alpha->get_resource_state() == ResourceState::LOADED) {
            alpha = "\033[32mY";
        } else {
            alpha = "\033[31mN";
        }
    }
    if(this->textures.depth != nullptr) {
        if(this->textures.depth->get_resource_state() == ResourceState::LOADED) {
            depth = "\033[32mY";
        } else {
            depth = "\033[31mN";
        }
    }
    return fmt::format("[{}{}{}{}{}{}\033[0m]", albedo, position, normal, specular,
        alpha, depth);
}
