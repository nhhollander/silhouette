#include "se/graphics/graphics.hpp"

#include "se/graphics/program.hpp"
#include "se/util/log.hpp"
#include "se/ecs/ecs.hpp"
#include "se/ecs/components.hpp"
#include "se/graphics/frameBuffer.hpp"
#include "se/graphics/geometry.hpp"
#include "se/util/opengl.hpp"

#include <SDL.h>
#include <GL/glew.h>
#include <queue>
#include <mutex>
#include <tuple>
#include <emscripten.h>
#include <emscripten/html5.h>

using namespace se::ecs::components;
using namespace se::util;
using namespace se::graphics;

namespace se::graphics {

    /// SDL window
    SDL_Window* window;

    /// SDL surface handle
    SDL_Surface* surface;

    /// Primary Framebuffer
    framebuffer::FrameBuffer* main_buffer;

    /*!
     * Screen Geometry.
     *
     * Screen geometry is just a square that covers the entire screen. This
     * is used to process the final render buffer(s) to the screen.
     */
    geometry::Geometry* screen_geometry;

    /*!
     * Screen Draw Program.
     */
    program::Program* screen_program;

    /**
     * OpenGL context.
     * 
     * This should only be used by components of the engine that are run on the
     * graphics thread.
     */
    SDL_GLContext gl_context;

    /**
     * Initialize SDL components.
     */
    void init_sdl();

    /**
     * Initialize SE components.
     */
    void init_se();

    /**
     * Start Rendering.
     * 
     * This function is meant to be called once all required base resources are
     * ready.
     */
    void start_rendering();

}

void se::graphics::init() {
    init_sdl();
    init_se();
}

void se::graphics::init_sdl() {
    DEBUG("Initializing SDL...");
    if(SDL_Init(SDL_INIT_VIDEO) < 0) {
        FATAL("Failed to initialize SDL! [%s]", SDL_GetError());
        SDL_ClearError();
        return;
    }

    DEBUG("Setting OpenGL version to [{}.{}]...",
        SE_OPENGL_VERSION_MAJOR, SE_OPENGL_VERSION_MINOR);
    if(SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, SE_OPENGL_VERSION_MAJOR) < 0) {
        WARN("Failed to set SDL_GL_CONTEXT_MAJOR_VERSION to {} [{}]",
            SE_OPENGL_VERSION_MAJOR, SDL_GetError());
        SDL_ClearError();
    }
    if(SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, SE_OPENGL_VERSION_MINOR) < 0) {
        WARN("Failed to set SDL_GL_CONTEXT_MINOR_VERSION to {} [{}]",
            SE_OPENGL_VERSION_MINOR, SDL_GetError());
        SDL_ClearError();
    }

    window = SDL_CreateWindow("WindowTitleGoesHere",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        640, 480,
        SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
    if(window == NULL) {
        FATAL("Failed to create SDL window! [{}]", SDL_GetError());
        SDL_ClearError();
        return;
    }

    gl_context = SDL_GL_CreateContext(window);
    if(gl_context == NULL) {
        FATAL("Failed to create OpenGL context! [{}]", SDL_GetError());
        SDL_ClearError();
        return;
    }

    glewExperimental = GL_TRUE;
    GLenum glew_error = glewInit();
    if(glew_error != GLEW_OK) {
        FATAL("Failed to initialize GLEW! [{}]",
            glewGetErrorString(glew_error));
        return;
    }

    if(SDL_GL_SetSwapInterval(1) < 0) {
        WARN("Failed to configure vsync [{}]", SDL_GetError());
        SDL_ClearError();
    }
    if(SDL_GL_GetSwapInterval() != 1) {
        // Warning for strange edge case on some systems
        WARN("Despite reporting success previously, vsync is not configured :(");
    }
}

void se::graphics::init_se() {
    DEBUG("Initializing Graphics");
    se::graphics::program::default_program = resource::get<program::Program>("default");
    
    auto check_ready = [](resource::Resource* _){
        // TODO: Maybe generate warnings on error?
        if(se::graphics::program::default_program->get_resource_state() != resource::ResourceState::LOADED) {
            return;
        }
        if(main_buffer->get_resource_state() != resource::ResourceState::LOADED) {
            return;
        }
        if(screen_program->get_resource_state() != resource::ResourceState::LOADED) {
            return;
        }
        if(screen_geometry->get_resource_state() != resource::ResourceState::LOADED) {
            return;
        }
        start_rendering();
    };

    main_buffer = resource::get<framebuffer::FrameBuffer>("main_fb", framebuffer::EnabledLayers::COLORD, 1920, 1080);

    screen_program = resource::get<program::Program>("screen_out");

    /* Simple box that covers the entire screen, with UV coordinates */
    geometry::Vertex raw_screen_vertices[] = {
        {
            {-1.0, -1.0,  0.0},
            { 0.0,  0.0},
            { 0.0,  0.0,  0.0}
        },
        {
            {-1.0,  1.0,  0.0},
            { 0.0,  1.0},
            { 0.0,  0.0,  0.0}
        },
        {
            { 1.0,  1.0,  0.0},
            { 1.0,  1.0},
            { 0.0,  0.0,  0.0}
        },
        {
            {-1.0, -1.0,  0.0},
            { 0.0,  0.0},
            { 0.0,  0.0,  0.0}
        },
        {
            { 1.0, -1.0,  0.0},
            { 1.0,  0.0},
            { 0.0,  0.0,  0.0}
        },
        {
            { 1.0,  1.0,  0.0},
            { 1.0,  1.0},
            { 0.0,  0.0,  0.0}
        }
    };
    screen_geometry = resource::get<geometry::Geometry>("output",
        std::vector<geometry::Vertex>(raw_screen_vertices,
        raw_screen_vertices + sizeof(raw_screen_vertices) / sizeof(geometry::Vertex))
    );

    se::graphics::program::default_program->add_state_change_listener(
        check_ready, resource::ResourceState::LOADED
    );
    main_buffer->add_state_change_listener(
        check_ready, resource::ResourceState::LOADED
    );
    screen_program->add_state_change_listener(
        check_ready, resource::ResourceState::LOADED
    );
    screen_geometry->add_state_change_listener(
        check_ready, resource::ResourceState::LOADED
    );

    se::graphics::program::default_program->increment_resource_user_count();
    main_buffer->increment_resource_user_count();
    screen_program->increment_resource_user_count();
    screen_geometry->increment_resource_user_count();

}

/**
* Emscripten loop wrapper.
*
* This method wraps the standard graphics loop method with an emscripten
* compatible method signature.
*/
inline int em_loop_wrapper(double time, void* data) {
    se::graphics::_loop();
    return EM_TRUE;
}

void se::graphics::start_rendering() {
    INFO("Registering render loop!");
    emscripten_request_animation_frame_loop(em_loop_wrapper, nullptr);

}

void se::graphics::deinit() {
}

void se::graphics::_loop() {

    opengl::clear();

    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // DEBUG
    if(!screen_program->use()) return;
    screen_geometry->draw();

    if(opengl::check_error()) {
        ERROR("Render loop encountered error!");
    }

    return;


    auto view = se::ecs::primary_registry.view<
        const Renderable,
        const Translate,
        const Scale,
        const Rotate
    >();

    main_buffer->use();

    view.each([](auto entity, auto renderable, auto translate, auto scale, auto rotate){
        
    });
}
