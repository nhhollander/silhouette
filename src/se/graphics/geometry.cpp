#include "se/graphics/geometry.hpp"

#include "se/graphics/geometry/loader.hpp"
#include "se/graphics/graphics.hpp"
#include "se/util/fileio.hpp"
#include "se/util/log.hpp"
#include "se/util/hash.hpp"
#include "se/util/opengl.hpp"

#include <map>
#include <SDL_opengl.h>
#include <GL/glew.h>

#define GEOMETRY_PATH "models/"

using namespace se::graphics::geometry;
using namespace se::util;

void se::graphics::geometry::Geometry::draw() {
    if(this->get_resource_state() != resource::ResourceState::LOADED) {
        WARN("[{}] Attempted to draw geometry before it was loaded ({})",
            this->get_extended_name(),
            resource::resource_state_name(this->get_resource_state()));
        return;
    }
    glBindVertexArray(this->vertex_array);
    glDrawArrays(GL_TRIANGLES, 0, this->vertex_count);
}

void se::graphics::geometry::Geometry::set_geometry(const std::vector<Vertex>& vertices){
    this->vertices = vertices;
}

std::string se::graphics::geometry::Geometry::calculate_extended_name(std::string name, std::vector<Vertex> vertices) {
    uint32_t geom_hash = se::util::hash::jenkins(vertices.data(), vertices.size() * sizeof(Vertex));
    return fmt::format("{}:{:04X}", name, geom_hash);
}

void se::graphics::geometry::Geometry::__configure(const std::vector<Vertex>& vertices) {
    this->local_geometry = true;
    this->vertices = vertices;
}

void se::graphics::geometry::Geometry::load_() {
    DEBUG("[{}] Loading", this->get_extended_name());
    this->set_state(resource::ResourceState::LOADING);

    auto abort = [this]() {
        this->set_state(resource::ResourceState::ERROR);
        this->vertices.clear();
    };

    auto loaded = [this,abort](const std::string& target, const std::vector<char>& raw, fileio::Result result, void* arg) {
        if(this->get_resource_state() != resource::ResourceState::LOADING) {
            WARN("[{}] Resource not in LOADING state! ({})", this->get_extended_name(),
                resource_state_name(this->get_resource_state()));
            return;
        }
        if(result == fileio::Result::FAILURE) {
            ERROR("[{}] Geometry file load failure!", this->get_extended_name());
            abort();
            return;
        }
        if(!geometry::decode(this->get_extended_name(), raw, this)) {
            ERROR("[{}] Failed to decode", this->get_extended_name());
            abort();
            return;
        }
        this->bind();
    };

    if(this->local_geometry) {
        this->bind();
    } else {
        std::string fname = GEOMETRY_PATH;
        fname += this->get_extended_name();
        fileio::read(fname, loaded);
    }
}

void se::graphics::geometry::Geometry::unload_() {
    this->set_state(resource::ResourceState::UNLOADING);

    glDeleteBuffers(1, &this->vertex_buffer);
    glDeleteVertexArrays(1, &this->vertex_array);

    this->vertex_array = (unsigned int) -1;
    this->vertex_buffer = (unsigned int) -1;

    this->set_state(resource::ResourceState::NOT_LOADED);
}

/* This method contains code adapted from the amazing model loading tutorial at
 * https://learnopengl.com/Model-Loading/Mesh */
void se::graphics::geometry::Geometry::bind() {

    if(this->get_user_count() == 0) {
        WARN("[{}] Ready to bind but user count is zero!", this->get_extended_name());
        return;
    }

    this->vertex_count = this->vertices.size();

    opengl::clear();

    glGenVertexArrays(1, &this->vertex_array);
    glGenBuffers(1, &this->vertex_buffer);

    glBindVertexArray(this->vertex_array);
    glBindBuffer(GL_ARRAY_BUFFER, this->vertex_buffer);

    glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex), this->vertices.data(), GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) 0);

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, uv));

    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, normal));

    glBindVertexArray(0);

    if(opengl::check_error()) {
        ERROR("[{}] Geometry failed to bind to graphics device!",
            this->get_extended_name());
        this->set_state(resource::ResourceState::ERROR);
    } else {
        this->set_state(resource::ResourceState::LOADED);
    }

}
