#include "se/graphics/texture.hpp"

#include "se/util/fileio.hpp"
#include "se/graphics/graphics.hpp"
#include "se/graphics/img/img.hpp"
#include "se/util/hash.hpp"
#include "se/util/log.hpp"
#include "se/util/opengl.hpp"

#include <cstdint>
#include <map>
#include <SDL_opengl.h>

#define TEXTURE_PATH "textures/"

using namespace se::util;
using namespace se::graphics::texture;

#define CASE(X) case X: return #X

const char* se::graphics::texture::pixelformat_to_str(PixelFormat fmt) {
    switch(fmt) {
        CASE(UNDEFINED);
        CASE(GRAY8);
        CASE(GRAY16);
        CASE(RGB8);
        CASE(RGB16);
        CASE(RGBA8);
        CASE(RGBA16);
        CASE(RGBF);
        CASE(RGBAF);
        CASE(DEPTH);
        default: return "<invalid PixelFormat>";
    }
}

/**
 * Get OpenGL format information.
 * 
 * Converts a `PixelFormat` structure into the set of internal values which
 * are expected by OpenGL. If an optional height and width are passed in, it
 * will also return the expected buffer size for sanity testing.
 * 
 * Values in this table adapted from
 * https://webgl2fundamentals.org/webgl/lessons/webgl-data-textures.html
 */
std::tuple<unsigned int, unsigned int, unsigned int, size_t> get_gl_format_info(PixelFormat fmt, int width = 0, int height = 0) {
    switch(fmt) {
        case GRAY8:
            return {
                GL_UNSIGNED_BYTE, // Type
                GL_RED,           // Format
                GL_R8,            // Internal Format
                width * height    // Expected raw image size in bytes
            };
        case GRAY16:
            return {
                GL_UNSIGNED_SHORT,
                GL_RED_INTEGER,
                GL_R16UI,
                width * height * 2
            };
        case RGB8:
            return {
                GL_UNSIGNED_BYTE,
                GL_RGB,
                GL_RGB,
                width * height * 3
            };
        case RGB16:
            return {
                GL_UNSIGNED_SHORT,
                GL_RGB_INTEGER,
                GL_RGB16UI,
                width * height * 6
            };
        case RGBA8:
            return {
                GL_UNSIGNED_BYTE,
                GL_RGBA,
                GL_RGBA8,
                width * height * 4
            };
        case RGBA16:
            return {
                GL_UNSIGNED_SHORT,
                GL_RGBA_INTEGER,
                GL_RGBA16UI,
                width * height * 8
            };
        case RGBF:
            return {
                GL_FLOAT,
                GL_RGB,
                GL_RGB32F,
                width * height * 12
            };
        case RGBAF:
            return {
                GL_FLOAT,
                GL_RGBA,
                GL_RGBA32F,
                width * height * 16
            };
        case DEPTH:
            return {
                GL_FLOAT,
                GL_DEPTH_COMPONENT,
                GL_DEPTH_COMPONENT32F,
                width * height * 4
            };
        default:
            // Also UNDEFINED
            // This is bad!
            return {0, 0, 0, 0};
        
    }
}

// Standard texture
std::string se::graphics::texture::Texture::calculate_extended_name(std::string name) {
    return name;
}

// Virtual texture
std::string se::graphics::texture::Texture::calculate_extended_name(std::string name, int dimx, int dimy, PixelFormat fmt) {
    /* Note: Resolution is variable, so it isn't included in the extended
     * name. If it was, resizing the window would create like 5 million
     * new resources and it would be hell. */
    return fmt::format("{}:{}", name, pixelformat_to_str(fmt));
}

// Standard texture
void se::graphics::texture::Texture::__configure() {
    this->virt = false;
}

// Virtual texture
void se::graphics::texture::Texture::__configure(int dimx, int dimy, PixelFormat fmt) {
    this->virt = true;
    this->width = dimx;
    this->height = dimy;
    this->pixel_format = fmt;
}

void se::graphics::texture::Texture::resize(int dimx, int dimy) {
    if(!this->virt) {
        ERROR("[{}] Non-virtual textures can not be resized", this->get_extended_name());
        return;
    }
    if(this->get_resource_state() != resource::ResourceState::LOADED) {
        this->width = dimx;
        this->height = dimy;
        // Reallocate the buffer
        const auto & [gl_type, gl_format, gl_internal_format, _] 
            = get_gl_format_info(this->pixel_format);

        glBindTexture(GL_TEXTURE_2D, this->id);
        glTexImage2D(GL_TEXTURE_2D, 0, gl_internal_format, this->height, this->width,
            0, gl_format, gl_type, NULL);
    } else {
        // Just set the value, it will be used next time a buffer is allocated
        this->width = dimx;
        this->height = dimy;
    }
}

void se::graphics::texture::Texture::load_() {
    DEBUG("[{}] Loading", this->get_extended_name());
    this->set_state(resource::ResourceState::LOADING);

    auto abort = [this]() {
        this->set_state(resource::ResourceState::ERROR);
        this->width = 0;
        this->height = 0;
        this->pixel_format = UNDEFINED;
        this->raw.clear();
        this->raw_encoded.clear();
    };

    auto loaded = [this,abort](const std::string& target, const std::vector<char>& raw, fileio::Result result, void* arg){
        if(this->get_resource_state() != resource::ResourceState::LOADING) {
            /* There are several reasons why this might occur, depending on the
             * actual state of the resource:
             *
             * resource::ResourceState::NOT_LOADED
             *  The resource was unloaded before the file could be downloaded.
             *  The user count probably dropped to 0 between the time the load
             *  was requested and the file was actually retrieved from the
             *  server.
             * 
             * resource::ResourceState::UNLOADING
             *  Basically the same as the `NOT_LOADED` state.
             * 
             * resource::ResourceState::LOADED
             *  The resource completed loading before the file could be
             *  downloaded. This is an unusual edge-case, which could only be
             *  caused by rapidly changing the user count from 0 » 1 » 0 » 1.
             *  This will result in two load requests being sent. The first one
             *  will (probably) complete as normal, and the second one will
             *  arrive late, causing this particular state.
             * 
             * resource::ResourceState::ERROR
             *  Basically the same as the `LOADED` state, but assuming that the
             *  resource encountered an error while loading.
             * 
             * In theory this is a very minor issue, and probably is not worth
             * the additional effort to add cancellation logic to the loading
             * process.
             */
            WARN("[{}] Resource not in LOADING state! ({})", this->get_extended_name(),
                resource_state_name(this->get_resource_state()));
            return;

        }
        if(result == fileio::Result::FAILURE) {
            ERROR("[{}] Texture load failure!",
                this->get_extended_name());
            abort();
            return;
        }
        this->raw_encoded = raw;
        if(!se::graphics::img::is_image(this)) {
            ERROR("[{}] Data is not an image!", this->get_extended_name());
            abort();
            return;
        }
        if(!se::graphics::img::load_image(this)) {
            ERROR("[{}] Image load failure!", this->get_extended_name());
            abort();
            return;
        }
        this->bind();
    };

    if(this->virt) {
        // Texture is virtual, skip loading
        this->bind();
    } else {
        // Fileio load
        std::string fname = TEXTURE_PATH;
        fname += this->get_extended_name();
        fileio::read(fname, loaded);
    }

}

void se::graphics::texture::Texture::unload_() {
    this->set_state(resource::ResourceState::UNLOADING);

    glBindTexture(GL_TEXTURE_2D, 0);
    glDeleteTextures(1, &this->id);
    this->set_state(resource::ResourceState::NOT_LOADED);
}

void se::graphics::texture::Texture::bind() {

    auto abort = [this]() {
        this->set_state(resource::ResourceState::ERROR);
        this->raw.clear();
    };

    if(this->pixel_format == UNDEFINED) {
        ERROR("[{}] Pixel format undefined!", this->get_extended_name());
        abort();
        return;
    }

    const auto & [gl_type, gl_format, gl_internal_format, expected_size] 
        = get_gl_format_info(this->pixel_format, this->width, this->height);

    // Sanity check (Only for non virtual textures)
    if(this->raw.size() != expected_size && !this->virt) {
        ERROR("[{}] Expected size != calculated size! Expected {} got {}",
            this->get_extended_name(), expected_size, this->raw.size());
        abort();
        return;
    }

    opengl::clear();

    // OpenGL
    glGenTextures(1, &this->id);
    glBindTexture(GL_TEXTURE_2D, this->id);
    // TODO: Make configurable, maybe by material parameters?
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    if(this->virt) {
        // Generate empty texture
        glTexImage2D(GL_TEXTURE_2D, 0, gl_internal_format, this->height, this->width,
            0, gl_format, gl_type, NULL);
    } else {
        // Generate texture from raw data
        glTexImage2D(GL_TEXTURE_2D, 0, gl_internal_format, this->height, this->width,
            0, gl_format, gl_type, this->raw.data());
    }

    this->raw.clear();

    if(opengl::check_error()) {
        ERROR("[{}] Texture failed to bind to graphics device!", this->get_extended_name());
        this->set_state(resource::ResourceState::ERROR);
    } else {
        this->set_state(resource::ResourceState::LOADED);
    }
}
