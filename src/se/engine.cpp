#include "se/engine.hpp"

#include "se/util/fileio.hpp"
#include "se/graphics/graphics.hpp"

#include "se/util/log.hpp"
#include "se/util/config.hpp"
#include "se/util/jsdebug.hpp"
#include "se/util/opengl.hpp"

#include <emscripten.h>
#include <queue>

/// Vector where pending tasks are stored
std::queue<se::engine::Job> job_queue;

void se::engine::init() {
    INFO("Initializing Silhouette Engine...");

    se::util::config::init();
    se::util::fileio::init();
    se::util::jsdebug::init();
    se::graphics::init();

    emscripten_set_main_loop(se::engine::loop, 0, false);
}

void se::engine::deinit() {
    INFO("De-initializing Silhouette Engine...");

    emscripten_cancel_main_loop();

    se::graphics::deinit();
}

void se::engine::loop() {

    if(!job_queue.empty()) {
        auto job = job_queue.front();
        job_queue.pop();
        job();
    }

}

void se::engine::register_job(se::engine::Job job) {
    job_queue.push(job);
}
