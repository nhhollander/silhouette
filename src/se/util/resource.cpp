#include "se/util/resource.hpp"

#include "se/util/log.hpp"
#include "se/util/string.hpp"

#include <algorithm>
#include <tuple>

#define CASE(X) case(X): return #X

using namespace se::util;
using namespace se::util::resource;

const char* se::util::resource::resource_state_name(ResourceState state) {
    switch(state) {
        CASE(NOT_LOADED);
        CASE(LOADING);
        CASE(UNLOADING);
        CASE(LOADED);
        CASE(ERROR);
        default: return "<Invalid ResourceState>";
    }
}

se::util::resource::Resource::Resource() {
    // No initialization required here
}

se::util::resource::Resource::~Resource() {
    ERROR("A resource has been deleted! Resources should never be deleted! "
        "Connect to debugger for more information.");
    /* TODO: Is there any way to identify the resource that has been deleted?
     * it appears that there is no way to call the virtual identification
     * functions for this class from the destructor. */
}

void se::util::resource::Resource::increment_resource_user_count() {
    this->resource_user_counter++;
    if(this->resource_user_counter == 1) {
        if(this->get_resource_state() != resource::ResourceState::NOT_LOADED) {
            WARN("[{}] Not in unloaded state! ({})",
                this->get_extended_name(),
                resource_state_name(this->get_resource_state()));
            return;
        }
        this->load_();
    }
}

void se::util::resource::Resource::decrement_resource_user_count() {
    if(this->resource_user_counter == 0) {
        WARN("Attempted to decrement user counter below zero!");
    } else {
        this->resource_user_counter--;
        if(this->resource_user_counter == 0) {
            if(this->get_resource_state() != resource::ResourceState::LOADED) {
                WARN("[{}] Not in loaded state! ({})",
                    this->get_extended_name(),
                    resource_state_name(this->get_resource_state()));
                this->set_state(resource::ResourceState::NOT_LOADED);
            } else {
                this->unload_();
            }
        }
    }
}

unsigned int se::util::resource::Resource::get_user_count() {
    return this->resource_user_counter;
}

void se::util::resource::Resource::set_state(ResourceState state) {
    this->resource_state = state;
    DEBUG("[{}] Stage changed to [{}]", this->get_extended_name(),
        resource_state_name(state));
    // Read from back to front to allow erasing without affecting the order of
    // un-processed elements.
    for(int i = this->state_change_listeners.size() - 1; i >= 0; i--) {
        ResourceStateChangeListener* l = &this->state_change_listeners[i];
        if(l->mask & state) {
            l->handler(this);
            this->state_change_listeners.erase(
                this->state_change_listeners.begin() + i
            );
        }
    }
}

void resource::Resource::add_state_change_listener(StateChangeHandler func, char trigger_mask) {
    this->state_change_listeners.push_back({
        func, trigger_mask
    });
}

void resource::Resource::__internal_configure(const std::string& name, const std::string& extended_name) {
    this->name = name;
    this->extended_name = extended_name;
}

std::map<size_t, se::util::resource::ResourceCache*> se::util::resource::global_cache;

std::map<size_t, std::string> se::util::resource::type_map;

void se::util::resource::generate_resource_status_report() {
    std::vector<std::tuple<std::string,std::string,Resource*>> all;
    for(const auto & [type, cache] : global_cache) {
        for(const auto & [name, resource] : *cache) {
            auto type_name = type_map.find(type)->second;
            type_name = string::demangle_class(type_name);
            all.push_back({type_name, name, resource});
        }
    }

    // Calculate values
    int resources = all.size();
    int resources_notloaded = 0;
    int resources_loading = 0;
    int resources_unloading = 0;
    int resources_loaded = 0;
    int resources_error = 0;
    for(const auto & [_, __, res] : all) {
        switch(res->get_resource_state()) {
            case(NOT_LOADED): resources_notloaded++; break;
            case(LOADING): resources_loading++; break;
            case(UNLOADING): resources_unloading++; break;
            case(LOADED): resources_loaded++; break;
            case(ERROR): resources_error++; break;
        }
    }
    std::string msg = "\nSilhouette Engine Resource Status Report:\n";
    msg.append(80, '-');
    msg += "\n";
    msg += "Total: \033[1;33m" + std::to_string(resources);
    msg += "\033[0m NOT_LOADED: \033[33m" + std::to_string(resources_notloaded);
    msg += "\033[0m LOADING: \033[33m" + std::to_string(resources_loading);
    msg += "\033[0m UNLOADING: \033[33m" + std::to_string(resources_unloading);
    msg += "\033[0m LOADED: \033[33m" + std::to_string(resources_loaded);
    msg += "\033[0m ERROR: \033[33m" + std::to_string(resources_error);
    msg += "\033[0m\n";
    msg.append(80, '-');
    msg += "\n\033[1m";
    const int typecol_width = 45;
    const int namecol_width = 35;
    const int statecol_width = 15;
    const int countcol_width = 5;
    msg += string::pad_right("Type", typecol_width, ' ');
    msg += string::pad_right("Name", namecol_width, ' ');
    msg += string::pad_right("State", statecol_width, ' ');
    msg += string::pad_right("Count", countcol_width, ' ');
    msg += "\033[0m\n";
    for(auto & [type, name, res] : all) {
        const char* color = "";
        std::string state = resource_state_name(res->get_resource_state());
        std::string count = std::to_string(res->get_user_count());
        type = string::pad_right(type, typecol_width, ' ', true);
        name = string::pad_right(name, namecol_width, ' ', true);
        state = string::pad_right(state, statecol_width, ' ', true);
        count = string::pad_left(count, countcol_width, ' ', true);
        switch(res->get_resource_state()) {
            case(LOADING):
                color = "\033[33m"; break;
            case(LOADED):
                color = "\033[32m"; break;
            case(ERROR):
                color = "\033[31m";
                break;
            default: break;
        }
        msg += color + type + name + state + count + "\033[0m\n";
    }
    msg.append(80, '-');
    DEBUG(msg.c_str());
}
