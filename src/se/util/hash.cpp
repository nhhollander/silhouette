#include "se/util/hash.hpp"

#include <cstdarg>
#include <stdio.h>

uint32_t se::util::hash::jenkins(const void* data, size_t len) {
    uint32_t hash = 0;
    // Digest the next byte
    for(size_t i = 0; i < len; i++) {
        hash += ((uint8_t*) data)[i];
        hash += hash << 10;
        hash ^= hash >> 6;
    }
    // Give the hash one final mixing about
    hash += hash << 3;
    hash ^= hash >> 11;
    hash += hash << 15;
    return hash;
}

uint32_t se::util::hash::p_jenkins(const char* fmt, ...) {
    // Prepare varargs
    va_list args;
    va_start(args, fmt);
    // Get message length for buffer size
    int len = vsnprintf(nullptr, 0, fmt, args);
    // Rewind varargs
    va_end(args);
    va_start(args, fmt);
    // Generate message
    char buf[len + 1];
    vsnprintf(buf, len+1, fmt, args);
    va_end(args);
    // Hash
    return jenkins(&buf[0], len);
}