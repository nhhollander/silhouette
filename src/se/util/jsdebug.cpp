#include "se/util/jsdebug.hpp"

#include "se/util/resource.hpp"
#include "se/util/log.hpp"

#include <emscripten.h>
#include <string.h>

using namespace se::util;

#define CASE(N) if(strcmp(name, #N) == 0) { se::util::jsdebug::N(arg); return; }

void se::util::jsdebug::init() {
    DEBUG("Initializing jsdebug System...");

    emscripten_run_script(
        "Module.jsdebug = {};"
        "Module.jsdebug.generate_resource_status_report = function() {"
        "   Module.cwrap('se_jsdebug','void',['string','string'])"
        "       ('generate_resource_status_report','')"
        "};"
    );
}

namespace se::util::jsdebug {

    /*!
     * Wrapper function to invoke the resource status report generator.
     */
    void generate_resource_status_report(const char*) {
        resource::generate_resource_status_report();
    }

}

extern "C" {

    /*!
     * Entry point function.
     *
     * All jsdebug commands are triggered through this exported function, in
     * order to simplify the compilation process. Since these functions are all
     * intended for debugging purposes, the extra overhead is not a concern.
     */
    void se_jsdebug(const char* name, const char* arg) {
        DEBUG("JSDEBUG {}: {}", name, arg);
        CASE(generate_resource_status_report)
        WARN("Unknown debug command received [{}]", name);
    }

}