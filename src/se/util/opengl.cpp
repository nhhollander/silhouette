#include "se/util/opengl.hpp"

#include "se/util/log.hpp"

#include <GL/glew.h>

// cSpell:ignoreRegExp GL[a-z]+ -- Ignore OpenGL identifiers

void se::util::opengl::clear() {
    if(check_error()) {
        WARN("The above OpenGL errors were not handled at the time of creation! "
            "They do not appear in the log chronologically.");
    }
}

bool se::util::opengl::check_error() {
    bool errors = false;
    GLenum code;
    while(true) {
        code = glGetError();
        if(code == GL_NO_ERROR) break;

        errors = true;

        const char* name = gl_error_name(code);
        const char* desc = gl_error_desc(code);

        ERROR("\033[1;96mOPENGL\033[0m {}: {}", name, desc);
    }

    return errors;
}

#define CASE(A) case(A): return #A

const char* se::util::opengl::gl_error_name(int error) {
    switch(error) {
        CASE(GL_NO_ERROR);
        CASE(GL_INVALID_ENUM);
        CASE(GL_INVALID_VALUE);
        CASE(GL_INVALID_OPERATION);
        CASE(GL_INVALID_FRAMEBUFFER_OPERATION);
        CASE(GL_OUT_OF_MEMORY);
        CASE(GL_STACK_UNDERFLOW);
        CASE(GL_STACK_OVERFLOW);
        default: return "<invalid GLenum>";
    }
}

const char* se::util::opengl::gl_error_desc(int error) {
    switch(error) {
        case(GL_NO_ERROR): return "No error has been recorded.";
        case(GL_INVALID_ENUM): return "An unacceptable value is specified for an enumerated argument.";
        case(GL_INVALID_VALUE): return "A numeric argument is out of range.";
        case(GL_INVALID_OPERATION): return "The specified operation is not allowed in the current state.";
        case(GL_INVALID_FRAMEBUFFER_OPERATION): return "The framebuffer object is not complete.";
        case(GL_OUT_OF_MEMORY): return "There is not enough memory left to execute the command.";
        case(GL_STACK_UNDERFLOW): return "An attempt has been made to perform an operation that would cause an internal stack to underflow.";
        case(GL_STACK_OVERFLOW): return "An attempt has been made to perform an operation that would cause an internal stack to overflow.";
        default: return "<invalid GLenum>";
    }
}
