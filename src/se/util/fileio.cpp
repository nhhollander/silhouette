#include "se/util/fileio.hpp"

#include "se/util/log.hpp"

#include <emscripten.h>
#include <sys/stat.h>

#define IDB_ROOT "/idb/"
#define SRV_ROOT "data/"
#define IDB_NAME "silhouette"
#define RETRY_COUNT 3

namespace se::util::fileio {

    /// Web request complete callback.
    void _wget_onload(unsigned handle, void* info, void* data, unsigned len);

    /// Web request error callback.
    void _wget_onerror(unsigned handle, void* info, int status, const char* strstatus);

    /// Web request progress callback.
    void _wget_onprogress(unsigned handle, void* _info, int loaded, int total);

    /// Indexed DB complete callback.
    void _idb_read_onload(void* info, void* data, int len);

    /// Indexed DB error callback.
    void _idb_read_onerror(void* info);

    /// Indexed DB write complete callback.
    void _idb_write_onstore(void* info);

    /// Indexed DB write error callback.
    void _idb_write_onerror(void* info);

    /**
     * Read callback information structure.
     *
     * This structure is used for passing information to the callback functions
     * via the emscripten asynchronous file APIs, since they are c-style and do
     * not support passing the entire request object through.
     */
    struct _rcbinfo {
        /// Path to the download target.
        std::string target = "";
        /// Callback function to invoke on read success/failure.
        se::util::fileio::rcallback callback;
        /// Pointer to an arbitrary user-specified argument.
        void* arg = nullptr;
        /// Retry counter, if applicable
        int retry_counter = -1;
    };

    /**
     * Write callback information structure.
     * 
     * See `_rcbinfo`
     */
    struct _wcbinfo {
        /// Path fo the download target.
        std::string target = "";
        /// Callback function to invoke on write success/failure.
        se::util::fileio::wcallback callback;
        /// Pointer to an arbitrary user-specified argument.
        void* arg = nullptr;
    };

    /**
     * Check if a file exists.
     */
    inline bool exists(const char* name);
}

#define CASE(X) case(X): return #X

const char* se::util::fileio::str_result(se::util::fileio::Result result) {
    switch(result) {
        CASE(SUCCESS);
        CASE(FAILURE);
        default: return "<invalid Result>";
    }
}

void se::util::fileio::init() {
    DEBUG("Mounting IndexDB");
    // Emscripten requires const here
    std::string idb_root = IDB_ROOT;
    EM_ASM({
        // Create the idb directory
        let idb_root_ = Module.UTF8ToString($0);
        FS.mkdir(idb_root_);
        // Mount IDBFS
        FS.mount(IDBFS, idb_root_);
        // Sync
        FS.syncfs(true, function(err){});
    }, idb_root.c_str());
}

void se::util::fileio::read(std::string target, se::util::fileio::rcallback callback, bool retry, void* arg) {
    /* Create the callback info structure. Remember to de-allocate this in the
     * callback handler!  Please! */
    int retry_ct = retry ? RETRY_COUNT : -1;
    _rcbinfo* info = new _rcbinfo({target, callback, arg, retry_ct});

    // Check for the file in the IndexDB
    std::string idb_path = IDB_ROOT + target;
    if(exists(idb_path.c_str())) {
        emscripten_idb_async_load(IDB_NAME, idb_path.c_str(), info,
            _idb_read_onload, _idb_read_onerror);
        return;
    }

    // Download the file from the host server
    std::string srvpath = SRV_ROOT + target;
    emscripten_async_wget2_data(srvpath.c_str(), "GET", "", info, true,
        _wget_onload, _wget_onerror, _wget_onprogress);
}

inline bool se::util::fileio::exists(const char* fname) {
    struct stat buffer;
    return stat(fname, &buffer) == 0;
}

void se::util::fileio::_wget_onload(unsigned handle, void* _info, void* data, unsigned len) {
    _rcbinfo* info = static_cast<_rcbinfo*>(_info);
    char* char_buf = (char*) data;
    info->callback(info->target, std::vector<char>(char_buf, char_buf + len), SUCCESS, info->arg);
    delete info;
}

void se::util::fileio::_wget_onerror(unsigned handle, void* _info, int status, const char* strstatus) {
    _rcbinfo* info = static_cast<_rcbinfo*>(_info);
    const char* description = (strstatus == nullptr) ? "<No Description>" : strstatus;
    ERROR("Failed to download [{}] from server! [{}: {}]", info->target,
        status, description);
    if(info->retry_counter > 0) {
        if(status >= 500 && status < 600) {
            DEBUG("Retrying [{}] ({}/{} attempts remaining)",
                info->target, info->retry_counter, RETRY_COUNT);
            info->retry_counter--;
            std::string srvpath = SRV_ROOT + info->target;
            emscripten_async_wget2_data(srvpath.c_str(), "GET", "", info, true,
                _wget_onload, _wget_onerror, _wget_onprogress);
            return;
        } else {
            DEBUG("Not retrying [{}], Unrecoverable error {}", info->target, status);
        }
    } else if(info->retry_counter == 0) {
        DEBUG("Not retrying [{}], Out of retries", info->target);
    } else {
        DEBUG("Not retrying [{}], Retries Disabled", info->target);
    }
    info->callback(info->target, std::vector<char>(), FAILURE, info->arg);
    delete info;
}

void se::util::fileio::_wget_onprogress(unsigned handle, void* _info, int loaded, int total) {
    // TODO: Best way to handle this?
}

void se::util::fileio::_idb_read_onload(void* _info, void* data, int len) {
    _rcbinfo* info = static_cast<_rcbinfo*>(_info);
    char* char_buf = (char*) data;
    info->callback(info->target, std::vector<char>(char_buf, char_buf + len), SUCCESS, info->arg);
    delete info;
}

void se::util::fileio::_idb_read_onerror(void* _info) {
    _rcbinfo* info = static_cast<_rcbinfo*>(_info);
    ERROR("Failed to read [{}] from IndexDB!", info->target);
    info->callback(info->target, std::vector<char>(), FAILURE, info->arg);
    delete info;
}

void se::util::fileio::__wrnoop(std::string,Result,void*) {};

template<> void se::util::fileio::write<std::vector<char>>(std::string target, std::vector<char> data, se::util::fileio::wcallback callback, void* arg) {
    _wcbinfo* info = new _wcbinfo({target, callback, arg});
    std::string idb_path = IDB_ROOT + target;
    emscripten_idb_async_store(IDB_NAME, idb_path.c_str(),
        (void*) data.data(), data.size(), info,
        _idb_write_onstore, _idb_write_onerror);
}

template<> void se::util::fileio::write<std::string>(std::string target, std::string data, se::util::fileio::wcallback callback, void* arg) {
    _wcbinfo* info = new _wcbinfo({target, callback, arg});
    std::string idb_path = IDB_ROOT + target;
    emscripten_idb_async_store(IDB_NAME, idb_path.c_str(),
        (void*) data.c_str(), data.size(), info,
        _idb_write_onstore, _idb_write_onerror);
}

// This is commented out only because doxygen was complaining about it.
// template<> void se::util::fileio::write<const char*>(std::string target, const char* data, se::util::fileio::wcallback callback, void* arg) {
//     _wcbinfo* info = new _wcbinfo({target, callback, arg});
//     std::string idb_path = IDB_ROOT + target;
//     emscripten_idb_async_store(IDB_NAME, idb_path.c_str(),
//         (void*) data, strlen(data), info,
//         _idb_write_onstore, _idb_write_onerror);
// }

void se::util::fileio::_idb_write_onstore(void* _info) {
    _wcbinfo* info = static_cast<_wcbinfo*>(_info);
    info->callback(info->target, SUCCESS, info->arg);
    delete info;
}

void se::util::fileio::_idb_write_onerror(void* _info) {
    _wcbinfo* info = static_cast<_wcbinfo*>(_info);
    ERROR("Failed to write to [{}]", info->target);
    info->callback(info->target, FAILURE, info->arg);
    delete info;
}
