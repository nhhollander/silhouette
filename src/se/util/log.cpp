#include "se/util/log.hpp"

#include <cstdarg>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <string>
#include <string.h>
#include <pthread.h>
#include <fmt/format.h>
#include <regex>
#include <emscripten/emscripten.h>

using namespace se::util::log;

#define CASE(X) case X: return #X
const char* se::util::log::level_to_str(se::util::log::Level level) {
    switch(level) {
        CASE(DEBUG);
        CASE(INFO);
        CASE(WARN);
        CASE(ERROR);
        CASE(FATAL);
        default: return "<Invalid Level>";
    }
}

/*!
 *  Retrieve the log format for a given level.
 * 
 *  This function will return a `printf` style format string to be used for
 *  printing messages to the console. Each level should have a unique style
 *  that can be used to quickly identify the severity of a message at a glance.
 * 
 *  @param level    The level to get the format for
 * 
 *  @return A format string if `level` is valid, otherwise a default message
 *  format.
 */
const char* getformat(Level level) {
    switch(level) {
        case DEBUG:  return "[\033[35mDEBUG\033[0m][\033[38;5;245m{srcfile}\033[0m:\033[32m{lineno}\033[0m:\033[38;5;245m{func}\033[0m] {message}\033[0m\n";
        case INFO:   return "[\033[38;5;27mINFO \033[0m][\033[38;5;245m{srcfile}\033[0m:\033[32m{lineno}\033[0m:\033[38;5;245m{func}\033[0m] {message}\033[0m\n";
        case WARN:   return "[\033[33mWARN \033[0m][\033[38;5;245m{srcfile}\033[0m:\033[32m{lineno}\033[0m:\033[38;5;245m{func}\033[0m] {message}\033[0m\n";
        case ERROR:  return "[\033[31mERROR\033[0m][\033[38;5;245m{srcfile}\033[0m:\033[32m{lineno}\033[0m:\033[38;5;245m{func}\033[0m] {message}\033[0m\n";
        case FATAL:  return "\033[41m[FATAL]\033[0m[\033[38;5;245m{srcfile}\033[0m:\033[32m{lineno}\033[0m:\033[38;5;245m{func}\033[0m] {message}\033[0m\n";
        default: return "[\033[32m?????\033[0m][{srcfile}:{lineno}:{func}] {message}\033[0m\n";
    };
}

/// ANSI format expression locator for replacement
auto ansi_re = std::regex("\033\\[(.*?)m");

EM_JS(void, se_console_log, (int level, const char* message_, const char* css_), {
    let message = UTF8ToString(message_);
    let css = UTF8ToString(css_);
    css = css.split("»");
    css.pop();
    switch(level) {
        case 2:
            console.warn(message, ...css); break;
        case 3:
        case 4:
            console.error(message, ...css); break;
        default:
            console.log(message, ...css); break;
    }
});

#define COLOR_BLACK     {  0,   0,   0}
#define COLOR_RED       {170,   0,   0}
#define COLOR_GREEN     {  0, 170,   0}
#define COLOR_YELLOW    {128, 128,   0}
#define COLOR_BLUE      {  0,   0, 170}
#define COLOR_MAGENTA   {170,   0, 170}
#define COLOR_CYAN      {  0, 170, 170}
#define COLOR_WHITE     {170, 170, 170}
#define COLOR_B_BLACK   { 85,  85,  85}
#define COLOR_B_RED     {155,  85,  85}
#define COLOR_B_GREEN   { 85, 255,  85}
#define COLOR_B_YELLOW  {255, 255,  85}
#define COLOR_B_BLUE    { 85,  85, 255}
#define COLOR_B_MAGENTA {255,  85, 255}
#define COLOR_B_CYAN    { 85, 255, 255}
#define COLOR_B_WHITE   {255, 255, 255}

std::tuple<int,int,int> decode_8b_color(int a) {
    if(a < 16) { // Standard 8 bit colors
        switch(a) {
            case(0):  return COLOR_BLACK;
            case(1):  return COLOR_RED;
            case(2):  return COLOR_GREEN;
            case(3):  return COLOR_YELLOW;
            case(4):  return COLOR_BLUE;
            case(5):  return COLOR_MAGENTA;
            case(6):  return COLOR_CYAN;
            case(7):  return COLOR_WHITE;
            case(8):  return COLOR_B_BLACK;
            case(9):  return COLOR_B_RED;
            case(10): return COLOR_B_GREEN;
            case(11): return COLOR_B_YELLOW;
            case(12): return COLOR_B_BLUE;
            case(13): return COLOR_B_MAGENTA;
            case(14): return COLOR_B_CYAN;
            case(15): return COLOR_B_WHITE;
            default: return {-1,-1,-1}; // If a is negative
        }
    } else if(a >= 231) { // Grayscale values
        int f = ((a - 232) / 23.0) * 255;
        return {f,f,f};
    } else { // 6x6x6 color cube
        int base = a - 16;
        int b = ((base % 6) / 6.0) * 255;
        int r = ((base / 36) / 6.0) * 255;
        int g = ((base % 36) / 6.0) * 255;
        return {r,g,b};
    }
}

/**
 * Print a string using the developer tools.
 * 
 * This method converts a message containing ANSI escape strings into a set of
 * arguments which can be passed to `console.log` in both chrome and firefox
 * using the style directives described at
 * https://developer.mozilla.org/en-US/docs/Web/API/console#usage.
 */
void print_devtools(int level, std::string message) {
    /* Generate the result string with ANSI sequences replaced by CSS formatting
     * indicators as defined in the web console api as described above. */
    std::string base = std::regex_replace(message, ansi_re, "%c");
    /* Find all ANSI escapes and convert them into CSS rules, keeping track of
     * previously set style information. */
    std::string css_rules;
    std::smatch match;
    while(std::regex_search(message, match, ansi_re)) {
        std::tuple<int,int,int> fg_color(-1, -1, -1);
        std::tuple<int,int,int> bg_color(-1, -1, -1);
        bool bold = false;
        bool italic = false;
        /* Extract the ANSI escape code values from the string and convert them
         * to integers for processing. */
        std::vector<int> numbers;
        size_t pos = 0;
        std::string mstring = match[1].str() + ";";
        while((pos = mstring.find(";")) != std::string::npos) {
            try {
                numbers.push_back(std::stoi(mstring.substr(0, pos)));
            } catch(const std::invalid_argument& _) {
                /* An invalid format component was given. There's really not
                 * much that we can do here, so we'll just replace it with an
                 * invalid ansi code and hope the user notices in the output. */
                numbers.push_back(-1);
            }
            mstring.erase(0, pos + 1);
        }
        message = match.suffix();
        // Process each of the ANSI codes for this segment, updating the styles
        for(int i = 0; i < numbers.size(); i++) {
            int color_type = 0; // Used for 8 and 24 bit color modes
            switch(numbers[i]) {
                // Reset formatting
                case(0):
                    fg_color = {-1, -1, -1};
                    bg_color = {-1, -1, -1};
                    bold = false;
                    italic = false; break;
                // Formatting
                case(1): bold = true; break;
                case(3): italic = true; break;
                // Foreground Color
                case(30):  fg_color = COLOR_BLACK;     break;
                case(31):  fg_color = COLOR_RED;       break;
                case(32):  fg_color = COLOR_GREEN;     break;
                case(33):  fg_color = COLOR_YELLOW;    break;
                case(34):  fg_color = COLOR_BLUE;      break;
                case(35):  fg_color = COLOR_MAGENTA;   break;
                case(36):  fg_color = COLOR_CYAN;      break;
                case(37):  fg_color = COLOR_WHITE;     break;
                case(90):  fg_color = COLOR_B_BLACK;   break;
                case(91):  fg_color = COLOR_B_RED;     break;
                case(92):  fg_color = COLOR_B_GREEN;   break;
                case(93):  fg_color = COLOR_B_YELLOW;  break;
                case(94):  fg_color = COLOR_B_BLUE;    break;
                case(95):  fg_color = COLOR_B_MAGENTA; break;
                case(96):  fg_color = COLOR_B_CYAN;    break;
                case(97):  fg_color = COLOR_B_WHITE;   break;
                // Background color
                case(40):  bg_color = COLOR_BLACK;     break;
                case(41):  bg_color = COLOR_RED;       break;
                case(42):  bg_color = COLOR_GREEN;     break;
                case(43):  bg_color = COLOR_YELLOW;    break;
                case(44):  bg_color = COLOR_BLUE;      break;
                case(45):  bg_color = COLOR_MAGENTA;   break;
                case(46):  bg_color = COLOR_CYAN;      break;
                case(47):  bg_color = COLOR_WHITE;     break;
                case(100): bg_color = COLOR_B_BLACK;   break;
                case(101): bg_color = COLOR_B_RED;     break;
                case(102): bg_color = COLOR_B_GREEN;   break;
                case(103): bg_color = COLOR_B_YELLOW;  break;
                case(104): bg_color = COLOR_B_BLUE;    break;
                case(105): bg_color = COLOR_B_MAGENTA; break;
                case(106): bg_color = COLOR_B_CYAN;    break;
                case(107): bg_color = COLOR_B_WHITE;   break;
                // Special foreground color modes
                case(38):
                    if(i+1 >= numbers.size()) break; // Not enough remaining
                    color_type = numbers[++i];
                    if(color_type == 2) { // 24 bit True Color mode
                        if(i+3 >= numbers.size()) break;
                        fg_color = {numbers[++i], numbers[++i], numbers[++i]};
                    } else if(color_type == 5) {
                        if(i+1 >= numbers.size()) break;
                        fg_color = decode_8b_color(numbers[++i]);
                    }
                    break;
                // Full-color background color
                case(48):
                    if(i+1 >= numbers.size()) break; // Not enough remaining
                    color_type = numbers[++i];
                    if(color_type == 2) { // 24 bit True Color mode
                        if(i+3 >= numbers.size()) break;
                        bg_color = {numbers[++i], numbers[++i], numbers[++i]};
                    } else if(color_type == 5) {
                        if(i+1 >= numbers.size()) break;
                        bg_color = decode_8b_color(numbers[++i]);
                    }
                    break;
                // Invalid case!
                default: break;
            }
        }
        // Convert the style rules into CSS
        if(std::get<0>(fg_color) > -1) {
            css_rules += fmt::format("color:rgb({},{},{});",
                std::get<0>(fg_color),
                std::get<1>(fg_color),
                std::get<2>(fg_color));
        }
        if(std::get<0>(bg_color) > -1) {
            css_rules += fmt::format("background-color:rgb({},{},{});",
                std::get<0>(bg_color),
                std::get<1>(bg_color),
                std::get<2>(bg_color));
        }
        if(bold) {
            css_rules += "font-weight:bold;";
        }
        if(italic) {
            css_rules += "font-style:italic;";
        }
        css_rules += "»";
    }
    se_console_log(level, base.c_str(), css_rules.c_str());
    
}

void se::util::log::log(Level level, const char* fname, int line, const char* func, std::string message) {
    try {
        print_devtools( level, fmt::format(getformat(level),
            fmt::arg("srcfile", fname),
            fmt::arg("lineno", line),
            fmt::arg("func", func),
            fmt::arg("message", message)
        ));
    } catch(...) {
        // This should *never* happen
        print_devtools(ERROR, fmt::format("Invalid message format for given arguments [{}]",
            fmt::format(getformat(level))
        ));
    }
}
