#include "se/util/config.hpp"

#include "se/util/log.hpp"

#include <emscripten.h>
#include <fmt/format.h>

void se::util::config::init() {
    int res = emscripten_run_script_int(
        // Test for availability of local storage
        "function test_available() {"
        "   try {"
        "       localStorage.setItem('se_storage_test_key','hello_world');"
        "       localStorage.removeItem('se_storage_test_key');"
        "       return 1;"
        "   } catch(e) {"
        "       return 0;"
        "   }"
        "};"
        "test_available()"
    );
    if(!res) {
        FATAL("Local storage is not available in this browser!");
    } else {
        DEBUG("Local storage is available!");
    }
}

EM_JS(char*, config_get, (const char* key), {
    let item = localStorage.getItem(UTF8ToString(key));
    if(!item) {
        return 0;
    } else {
        let len = lengthBytesUTF8(item) + 1;
        let heap_ptr = _malloc(len);
        stringToUTF8(item, heap_ptr, len);
        return heap_ptr;
    }
});

EM_JS(void, config_set, (const char* key, const char* value), {
    let key_ = UTF8ToString(key);
    let value_ = UTF8ToString(value);
    localStorage.setItem(key_, value_);
});

EM_JS(void, config_unset, (const char* key), {
    localStorage.removeItem(UTF8ToString(key));
});


// =========== //
// = GETTERS = //
// =========== //

std::string se::util::config::get_str(std::string key, std::string def) {
    char* value = config_get(key.c_str());
    if(value == nullptr) {
        return def;
    }
    std::string result = std::string(value);
    free((void*) value);
    return result;
}

int se::util::config::get_int(std::string key, int def) {
    char* value = config_get(key.c_str());
    int res = def;
    if(value != nullptr) {
        try {
            res = std::stoi(value);
        } catch(const std::invalid_argument& e) {
            WARN("Failed to parse [{}]:[{}] as [int]", key, value);
        } catch(const std::out_of_range& e) {
            WARN("Value of [{}]:[{}] out of range for [int]", key, value);
        }
        free((void*) value);
    }
    return res;
}

long se::util::config::get_long(std::string key, long def) {
    char* value = config_get(key.c_str());
    long res = def;
    if(value != nullptr) {
        try {
            res = std::stol(value);
        } catch(const std::invalid_argument& e) {
            WARN("Failed to parse [{}]:[{}] as [long]", key, value);
        } catch(const std::out_of_range& e) {
            WARN("Value of [{}]:[{}] out of range for [long]", key, value);
        }
        free((void*) value);
    }
    return res;
}

long long se::util::config::get_long_long(std::string key, long long def) {
    char* value = config_get(key.c_str());
    long long res = def;
    if(value != nullptr) {
        try {
            res = std::stoll(value);
        } catch(const std::invalid_argument& e) {
            WARN("Failed to parse [{}]:[{}] as [long long]", key, value);
        } catch(const std::out_of_range& e) {
            WARN("Value of [{}]:[{}] out of range for [long long]", key, value);
        }
        free((void*) value);
    }
    return res;
}

float se::util::config::get_float(std::string key, float def) {
    char* value = config_get(key.c_str());
    float res = def;
    if(value != nullptr) {
        try {
            res = std::stof(value);
        } catch(const std::invalid_argument& e) {
            WARN("Failed to parse [{}]:[{}] as [float]", key, value);
        } catch(const std::out_of_range& e) {
            WARN("Value of [{}]:[{}] out of range for [float]", key, value);
        }
        free((void*) value);
    }
    return res;
}

double se::util::config::get_double(std::string key, double def) {
    char* value = config_get(key.c_str());
    double res = def;
    if(value != nullptr) {
        try {
            res = std::stod(value);
        } catch(const std::invalid_argument& e) {
            WARN("Failed to parse [{}]:[{}] as [double]", key, value);
        } catch(const std::out_of_range& e) {
            WARN("Value of [{}]:[{}] out of range for [double]", key, value);
        }
        free((void*) value);
    }
    return res;
}

// TODO: Maybe there's a better way to do this? More versatile?
bool se::util::config::get_bool(std::string key, bool def) {
    char* value = config_get(key.c_str());
    bool res = def;
    if(value != nullptr) {
        try {
            res = std::stod(value) != 0.0;
        } catch(const std::invalid_argument& e) {
            WARN("Failed to parse [{}]:[{}] as [bool] (via double)", key, value);
        } catch(const std::out_of_range& e) {
            WARN("Value of [{}]:[{}] out of range for [bool] (via double)", key, value);
        }
        free((void*) value);
    }
    return res;
}

// =========== //
// = SETTERS = //
// =========== //

void se::util::config::set(std::string key, std::string value) {
    config_set(key.c_str(), value.c_str());
}

void se::util::config::set(std::string key, const char* value) {
    config_set(key.c_str(), value);
}

void se::util::config::set(std::string key, int value) {
    config_set(key.c_str(), std::to_string(value).c_str());
}

void se::util::config::set(std::string key, long value) {
    config_set(key.c_str(), std::to_string(value).c_str());
}

void se::util::config::set(std::string key, long long value) {
    config_set(key.c_str(), std::to_string(value).c_str());
}

void se::util::config::set(std::string key, float value) {
    config_set(key.c_str(), std::to_string(value).c_str());
}

void se::util::config::set(std::string key, double value) {
    config_set(key.c_str(), std::to_string(value).c_str());
}

void se::util::config::set(std::string key, bool value) {
    config_set(key.c_str(), std::to_string(value).c_str());
}

// ========= //
// = OTHER = //
// ========= //

void se::util::config::unset(std::string key) {
    config_unset(key.c_str());
}
