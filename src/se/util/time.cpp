#include "se/util/time.hpp"

/* Yeah it's a little bit silly to have an entire source file that just contains
 * three variables, but that's what it takes in order for my janky little
 * homemade inline profiler to work. */

std::chrono::high_resolution_clock::time_point se::util::time::now;
std::chrono::high_resolution_clock::time_point se::util::time::tick;
std::chrono::high_resolution_clock::time_point se::util::time::tock;
