#include "se/util/string.hpp"

#include "se/util/log.hpp"

#include <typeinfo>
#include <cxxabi.h>

std::string se::util::string::pad_left(std::string string, int len, char pad, bool trim) {
    int count = len - string.length();
    if(count < 0 && trim) {
        // Trim string
        return string.substr(-count, len);
    } else if(count > 0) {
        // Pad string
        std::string nstring = std::string(string);
        nstring.insert(nstring.begin(), count, pad);
        return nstring;
    } else {
        // No modification
        return string;
    }
}

std::string se::util::string::pad_right(std::string string, int len, char pad, bool trim) {
    int count = len - string.length();
    if(count < 0 && trim) {
        // Trim string
        return string.substr(0, len);
    } else if(count > 0) {
        // Pad string
        std::string nstring = std::string(string);
        nstring.append(count, pad);
        return nstring;
    } else {
        // No modification
        return string;
    }
}

std::string se::util::string::demangle_class(std::string class_name) {

    int status = 0;
    const char* demangled = abi::__cxa_demangle(class_name.c_str(), NULL, NULL, &status);
    if(status != 0) {
        const char* err_string = nullptr;
        switch(status) {
            case -1: err_string = "A memory allocation failure occurred."; break;
            case -2: err_string = "Not a valid mangled name"; break;
            case -3: err_string = "One of the arguments is invalid"; break;
            default: err_string = "Unknown error";
        }
        WARN("Unable to demangle class name [{}], {}: {}",
            class_name, status, err_string);
        return class_name;
    } else {
        return std::string(demangled);
    }

}