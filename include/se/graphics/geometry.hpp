/** \file
 * Silhouette Engine Geometry.
 *
 * Geometry is defined as a series of points which make up triangles which in
 * turn make up some sort of thing that can be rendered.
 * 
 * Copyright 2021 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

// Forward declaration required to handle circular reference
namespace se::graphics::geometry {
    class Geometry;
}

#pragma once

#include "se/util/resource.hpp"

#include <vector>
#include <string>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>

/**
 * Geometry Namespace.
 *
 * This namespace contains relevent functions and definitions for the
 * Geometry class.
 */
namespace se::graphics::geometry {

    /*!
     * Vertex Structure.
     *
     * The vertex structure allows vertices to be efficiently packed in memory
     * in the same arrangement expected by the OpenGL specification.
     */
    struct Vertex {
        /// Position of the vertex in the mesh
        glm::vec3 position;
        /// UV coordinates of this vertex
        glm::vec2 uv;
        /// Normal value at this vertex
        glm::vec3 normal;
    };

    /**
     * Geometry Class.
     *
     * This class describes a single geometry object bound to the system.
     */
    class Geometry : public se::util::resource::Resource {

        public:

            /**
             * Draw the geometry.
             */
            void draw();

            /*!
             * Set the geometric points.
             *
             * This method is intended to be used by the geometry loading system
             * to assign the calculated mesh data array to this geometry object.
             * 
             * A rebind operation is not triggered by calling this method.
             */
            void set_geometry(const std::vector<Vertex>& vertices);

            /**
             * Calculate the extended name.
             * 
             * Used for hashing and cache retrieval.
             */
            static std::string calculate_extended_name(std::string name) {
                return name;
            }

            /**
             * Calculate the extended name for predefined geometry.
             */
            static std::string calculate_extended_name(std::string name, std::vector<Vertex> vertices);

            /**
             * Initialization Configuration Method.
             * 
             * This method must exist, but it doesn't have to do anything.
             */
            virtual void __configure() {};

            /**
             * Initialization Configuration Method for predefined geometry.
             */
            void __configure(const std::vector<Vertex>& vertices);

        private:

            /// Un-bound vertex values
            std::vector<Vertex> vertices;

            /// Geometry specified locally
            bool local_geometry = false;

            /// @see `se::util::resource::Resource`
            void load_() override;

            /// @see `se::util::resource::Resource`
            void unload_() override;

            /**
             * Bind the geometry to the graphics device.
             * 
             * This method must only be called from the graphics thread.
             */
            void bind();

            /*!
             * OpenGL vertex array ID.
             */
            unsigned int vertex_array = (unsigned int) -1;

            /*!
             * OpenGL vertex buffer ID.
             */
            unsigned int vertex_buffer = (unsigned int) -1;

            /*!
             * Vertex count.
             */
            unsigned int vertex_count = 0;
    };

}