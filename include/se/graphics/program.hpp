/** \file
 * Silhouette Engine Shader Program.
 *
 * Shader programs represent a collection of shaders, and form the executable
 * component of a single stage of the render pipeline. More information can be
 * found on the [Khronos Wiki](https://www.khronos.org/opengl/wiki/GLSL_Object).
 * 
 * Copyright 2020 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

#pragma once

#include "se/graphics/shader.hpp"
#include "se/util/resource.hpp"

#include <cstdint>
#include <string>

/**
 * Program Namespace.
 *
 * This namespace contains relevent functions and definitions for loading and
 * managing OpenGL programs via the Program resource class.
 */
namespace se::graphics::program {

    /**
     * Program Class.
     *
     * This class describes a single program object. Programs are made up of
     * shaders, which collectively perform one stage of a rendering operation.
     */
    class Program : public se::util::resource::Resource {

        public:
     
            /**
             * Fragment Shader.
             */
            se::graphics::shader::Shader* frag = nullptr;

            /**
             * Vertex Shader.
             */
            se::graphics::shader::Shader* vert = nullptr;

            /**
             * Make this the active program.
             * 
             * Returns `false` if the program is not ready to be used.
             */
            bool use();

            /**
             * Calculate the extended name.
             * 
             * Used for hashing and cache retrieval.
             */
            static std::string calculate_extended_name(std::string name) {
                return name;
            }

            /**
             * Initialization Comfiguration Method.
             */
            void __configure();

        private:

            /// @see `se::util::resource::Resource`
            void load_() override;

            /// @see `se::util::resource::Resource`
            void unload_() override;

            /**
             * Link the component shaders.
             * 
             * This method must only be called from the graphics thread.
             */
            void link();

            /**
             * OpenGL Identifier.
             *
             * Used by OpenGL to specify which program should be used. This
             * variable is only useful within the graphics thread, and should
             * not be used externally.
             */
            unsigned int id = 0;
    };

    /**
     * Default Program.
     * 
     * This program will be used whenever `use_program()` is called on a program
     * that is not ready to be used, or is in an errored state.
     */
    extern Program* default_program;

}