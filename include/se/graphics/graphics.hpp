/** \file
 * This is the Silhouette Engine graphics subsystem.
 * 
 * Copyright 2020 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

#pragma once

// WebGL 2.0 is a subset of OpenGL ES 3.0
/// OpenGL Major Version (3 Required for WebGL2)
#define SE_OPENGL_VERSION_MAJOR 3
/// OpenGL Minor Version (0 Required for WebGL2)
#define SE_OPENGL_VERSION_MINOR 0

#include <functional>
#include <string>

/**
 * Primary Graphics Namespace.
 *
 * This namespace contains the primary defintions used by the graphics subsystem
 * of the engine.
 */
namespace se::graphics {

    /**
     * Graphics thread initialization method.
     * 
     * This method initializes the graphics system on the current thread.
     */
    void init();

    /**
     * Graphics thread de-initialization method.
     * 
     * This method de-initializes the graphics system on the current thread.
     */
    void deinit();

    /**
     * Graphics thread loop method.
     * 
     * This method is called for every frame that is to be drawn. It should be
     * passed to whichever scheduling system is responsible for 
     */
    void _loop();

}