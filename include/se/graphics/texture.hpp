/** \file
 * Textures.
 * 
 * Copyright 2021 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

#pragma once

#include "se/util/resource.hpp"

#include <string>
#include <cstdint>
#include <vector>

/**
 * Texture Namespace.
 *
 * This namespace contains relevent functions and definitions for loading and
 * managing OpenGL textures via the Texture resource class.
 */
namespace se::graphics::texture {

    /// Pixel Format
    enum PixelFormat {
        UNDEFINED,
        /// Monochrome 8 bit
        GRAY8,
        /// Monochrome 16 bit
        GRAY16,
        /// Color 8 bit
        RGB8,
        /// Color 16 bit
        RGB16,
        /// Color + alpha 8 bit
        RGBA8,
        /// Color + alpha 16 bit
        RGBA16,
        /// Color 32 bit float
        RGBF,
        /// Color + alpha 32 bit float
        RGBAF,
        /// Depth
        DEPTH
    };

    /// Pixel format to string
    const char* pixelformat_to_str(PixelFormat fmt);

    /**
     * Texture Class.
     */
    class Texture : public se::util::resource::Resource {

        public:

            /// Raw Texture Data (Decoded)
            std::vector<unsigned char> raw;

            /// Raw Texture Data (Encoded)
            std::vector<char> raw_encoded;

            /// Pixel Format
            PixelFormat pixel_format = UNDEFINED;

            /// Texture width
            uint32_t width = 0;

            /// Texture height
            uint32_t height = 0;

            /**
             * Texture is Virtual.
             *
             * Virtual textures exist only in memory, instead of being loaded
             * from files. They are generally used for procedural textures or as
             * render targets.
             */
            bool virt = false;

            /// OpenGL ID.
            unsigned int id = 0;

            /**
             * Calculate the extended name (For standard textures).
             * 
             * Used for hashing and cache retrieval.
             */
            static std::string calculate_extended_name(std::string name);

            /**
             * Calculate the extended name (For virtual textures).
             * 
             * Used for hashing and cache retrieval.
             */
            static std::string calculate_extended_name(std::string name, int dimx, int dimy, PixelFormat fmt);

            /**
             * Initialization Comfiguration Method (For standard textures).
             */
            void __configure();

            /**
             * Initialization Configuration Method (For virtual textures).
             * 
             * *Note:* The resolution given will only be used if the texture
             * has not already been initialized. If it has been initialized,
             * these values are discarded.
             */
            void __configure(int dimx, int dimy, PixelFormat fmt);

            /**
             * Resize the texture.
             * 
             * The texture will be re-initialized with a new resolution. This
             * operation is effectively instantaneous, and resizes the
             * underlying buffer without changing the OpenGL identifier.
             * 
             * *Note:* This operation can only be performed on virtual textures.
             */
            void resize(int dimx, int dimy);

        private:

            /// @see `se::util::resource::Resource`
            void load_() override;

            /// @see `se::util::resource::Resource`
            void unload_() override;

            /**
             * Bind the texture to the graphics device.
             * 
             * This method must only be called from the graphics thread.
             */
            void bind();

    };

}