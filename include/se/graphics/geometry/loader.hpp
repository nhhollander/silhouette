/** \file
 * Geometry Loader.
 * 
 * Copyright 2020 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

#pragma once

#include "se/graphics/geometry.hpp"

#include <vector>
#include <string>

#pragma once

namespace se::graphics::geometry {

    /*!
     * Decode geometry.
     *
     * This method automatically detects geometry/model type and returns the
     * appropriately decoded data, or an error, depending on the result of the
     * operation.
     *
     * @param name     Name of the object file resource.
     * @param data     Unprocessed contents of the model file.
     * @param geom     Geometry object to store the loaded model data in.
     */
    bool decode(std::string name, std::vector<char> data, Geometry* geom);

}