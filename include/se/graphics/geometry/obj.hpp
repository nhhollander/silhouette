/** \file
 * OBJ File Loader
 * 
 * Copyright 2021 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

#pragma once

#include "se/graphics/geometry.hpp"

#include <string>
#include <vector>

namespace se::graphics::geometry {

    /*!
     * Parse an OBJ file.
     *
     * @param name     Name of the object file resource.
     * @param raw_data Unprocessed contents of the model file.
     * @param geom     Geometry object to store the loaded model data in.
     */
    bool load_obj(std::string name, std::vector<char> raw_data, se::graphics::geometry::Geometry* geom);

}