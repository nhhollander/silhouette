/** \file
 * Silhouette Engine Shader Code.
 *
 * Shaders are an essential component of the OpenGL graphics pipeline. Shaders
 * represent a single stage of the rendering process. More information can be
 * found on the [Khronos Wiki](https://www.khronos.org/opengl/wiki/Shader).
 * 
 * Copyright 2020 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

#pragma once

#include "se/util/resource.hpp"

#include <cstdint>
#include <string>

/**
 * Shader Namespace.
 *
 * This namespace contains relevent functions and definitions for loading and
 * managing OpenGL shaders via the Shader resource class.
 */
namespace se::graphics::shader {

    /**
     * Shader Class.
     * 
     * This class describes a single shader object. This class should never be
     * passed by value, use `load_shader` to get an instance from the cache.
     */
    class Shader : public se::util::resource::Resource {

        public:

            /**
             * Uncompiled Shader Source.
             *
             * Used to store the source code of the shader after it has been
             * loaded, but before it has been passed to the graphics thread for
             * compilation. There is no guarantee that this variable contains
             * anything after compilation.
             */
            std::vector<char> src;

            /**
             * Calculate the extended name.
             * 
             * Used for hashing and cache retrieval.
             */
            static std::string calculate_extended_name(std::string name, int type);

            /**
             * Initialization Comfiguration Method.
             */
            void __configure(int type);

            /**
             * Get the shader ID
             */
            inline unsigned int get_id() { return this->id; }

        private:

            /**
             * Shader Type.
             * 
             * OpenGL shader type identifier.
             */
            unsigned int type = 0;

            /**
             * OpenGL Identifier.
             *
             * Used by OpenGL to identify this shader in program operations.
             * This variable is only useful within the graphics thread, and
             * should not be used externally.
             */
            unsigned int id = 0;

            /// @see `se::util::resource::Resource`
            void load_() override;

            /// @see `se::util::resource::Resource`
            void unload_() override;

            /**
             * Compile Shader.
             */
            void compile();

    };

}