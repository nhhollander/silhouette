/** \file
 * Image Helper Utility.
 * 
 * Copyright 2021 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

#pragma once

#include "se/graphics/img/png.hpp"

/**
 * Image Loading Namespace.
 * 
 * This namespace contains the image loader for textures.
 */
namespace se::graphics::img {

    /**
     * Check if image.
     * 
     * This function attempts to determine if the provided file data is actually
     * an image of any recognized type.
     */
    bool is_image(se::graphics::texture::Texture* texture);

    /**
     * Try all image types and load.
     *
     * This function attempts to load the provided data as any one of the
     * supported image types.
     *
     * @return `false` on failure
     */
    bool load_image(se::graphics::texture::Texture* texture);

}