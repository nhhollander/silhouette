/** \file
 * PNG Helper Utility.
 * 
 * Copyright 2021 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

#pragma once

#include "se/graphics/texture.hpp"

#include <vector>

namespace se::graphics::img {

    /**
     * Check if PNG.
     * 
     * This function attempts to determine if the provided file data is actually
     * a PNG.
     */
    bool is_png(se::graphics::texture::Texture* texture);

    /**
     * Load a PNG.
     * 
     * This function attempts to interpret the input data as a PNG file.
     * 
     * @return `false` on failure
     */
    bool load_png(se::graphics::texture::Texture* texture);

}