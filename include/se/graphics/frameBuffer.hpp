/** \file
 * Engine render targets, aka frame buffers.
 * 
 * Copyright 2021 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

#pragma once

#include "se/graphics/texture.hpp"
#include "se/util/resource.hpp"

// cSpell:ignoreRegExp (?:GRAY[A-Z]?|RGB[A-Z]?)

/**
 * FrameBuffer Namespace.
 *
 * This namespace contains relevent functions and definitions for the
 * FrameBuffer class.
 */
namespace se::graphics::framebuffer {

    /**
     * Buffer Layer Flags.
     */
    enum LayerFlag {
        // BASE LAYERS //
        ALBEDO =   0b00000001,
        POSITION = 0b00000010,
        NORMAL =   0b00000100,
        SPECULAR = 0b00001000,
        ALPHA =    0b00010000,
        DEPTH =    0b00100000,
    };

    /**
     * EnabledLayers.
     */
    enum EnabledLayers {
        /// Base, minimum required layers for rendering
        COLOR = ALBEDO | POSITION | NORMAL | SPECULAR,
        /// Color + Alpha
        COLORA = COLOR | ALPHA,
        /// Color + Depth
        COLORD = COLOR | DEPTH,
        /// Color + Alpha + Depth
        COLORAD = COLOR | ALPHA | DEPTH
        // TODO: Stencil
    };

    /**
     * `LayerFlag` to string.
     * 
     * Converts a `LayerFlag` value into its string representation, useful for
     * debugging.
     */
    const char* layerflag_to_str(LayerFlag flag);

    /**
     * `EnabledLayers` as string.
     * 
     * Converts an `EnabledLayers` value into its string representation, useful
     * for debugging.
     */
    const char* enabledlayers_to_str(EnabledLayers layers);

    /**
     * Buffer Textures.
     *
     * This structure contains pointers to the respective buffer textures.
     */
    struct BufferTextures {
        /// RGB
        se::graphics::texture::Texture* albedo = nullptr;
        /// Position
        se::graphics::texture::Texture* position = nullptr;
        /// Normal
        se::graphics::texture::Texture* normal = nullptr;
        /// Specular
        se::graphics::texture::Texture* specular = nullptr;
        /// Alpha mask
        se::graphics::texture::Texture* alpha = nullptr;
        /// Depth buffer
        se::graphics::texture::Texture* depth = nullptr;
    };

    /**
     * Framebuffer Class.
     * 
     * Frame buffers represent a collection of textures that can be rendered to
     * by the engine.
     */
    class FrameBuffer : public se::util::resource::Resource {

        public:

            /**
             * Calculate the extended name.
             * 
             * Used for hashing and cache retrieval.
             */
            static std::string calculate_extended_name(std::string name, EnabledLayers layers, int default_width, int default_height);

            /**
             * Initialization Comfiguration Method.
             */
            void __configure(EnabledLayers layers, int default_width, int default_height);

            /**
             * Make this framebuffer active.
             * 
             * If the buffer is not ready to use, a warning will be generated
             * and this method will return false. Rendering which depends on the
             * presence of a framebuffer should not proceed.
             */
            bool use();
        
        private:

            /// @see `se::util::resource::Resource`
            void load_() override;

            /// @see `se::util::resource::Resource`
            void unload_() override;

            /// Bind the framebuffer
            void bind();

            /// OpenGL ID.
            unsigned int id = 0;

            /// Textures
            BufferTextures textures;

            /*!
             * Check texture status.
             *
             * Checks if all of the enabled textures are in the LOADED state.
             */
            bool textures_ready();

            /*!
             * Check texture status.
             *
             * Returns a string representing the current status.
             */
            std::string textures_status();
    };

}