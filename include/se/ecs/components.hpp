/** \file
 * Standard components.
 * 
 * Copyright 2021 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "se/graphics/program.hpp"
#include "se/graphics/geometry.hpp"

/**
 * ECS Components Namespace.
 * 
 * This namespace contains structure definitions for the built-in types used by
 * the engine for various tasks.
 * 
 * *Note: At some point it might be a good idea to break this out into separate
 * component definition files for each system, i.e. a graphics components file,
 * an input components file, a logic components file, etc.*
 */
namespace se::ecs::components {

    /*!
     * Renderable Component.
     *
     * This structure represents the basic information needed by renderable
     * entities. It specifies basic information about a renderable object,
     * which can be interpreted during the render phase to draw it
     * appropriately.
     */
    struct Renderable {
        /// Rendering enabled
        bool enabled;
        /// Selected Program
        se::graphics::program::Program* program;
        /// Program GL identifier for faster access
        unsigned int program_id = (unsigned int) -1;
        /// Geometry
        se::graphics::geometry::Geometry* geometry;
        /// Graphics GL identifier for faster access
        unsigned int geometry_id = (unsigned int) -1;
    };

    /*!
     * Translate Component.
     *
     * Represents an objects position in 3D space.
     */
    struct Translate {
        /// X Position
        float x = 0.0f;
        /// Y Position
        float y = 0.0f;
        /// Z Position
        float z = 0.0f;
    };

    /*!
     * Scale Component.
     *
     * Represents the scale of an object along all 3 axes (xyz)
     */
    struct Scale {
        /// X Scale
        float x = 0.0f;
        /// Y Scale
        float y = 0.0f;
        /// Z Scale
        float z = 0.0f;
    };

    /*!
     * Rotation Component.
     *
     * Represents the rotation of an object around all 3 axes as x-y'-z''
     * Tait-Byran angles (αβγ or φθψ)
     */
    struct Rotate {
        /// Rotation about the X axis (α/φ)
        float x = 0.0f;
        /// Rotation about the Y axis (β/θ)
        float y = 0.0f;
        /// Rotation about the Z axis (γ/ψ)
        float z = 0.0f;
    };

    /**
     * Transformation Component.
     *
     * Represents an arbitrary 3D transformation consisting of position, scale,
     * and rotation.
     */
    struct Transform {
        /// X Position
        float pos_x = 0.0f;
        /// Y Position
        float pos_y = 0.0f;
        /// Z Position
        float pos_z = 0.0f;

        /// Scale along X axis
        float scale_x = 0.0f;
        /// Scale along Y axis
        float scale_y = 0.0f;
        /// Scale along Z axis
        float scale_z = 0.0f;

        /// Rotation about X axis (radians) α
        float rot_x = 0.0f;
        /// Rotation about Y axis (radians) β
        float rot_y = 0.0f;
        /// Rotation about Z axis (radians) γ
        float rot_z = 0.0f;
    };
}
