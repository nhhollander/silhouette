/** \file
 * Silhouette Engine Data Management.
 * 
 * Copyright 2020 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

#pragma once

#include <entt/entity/registry.hpp>
#include <mutex>

/**
 * Main ECS Namespace.
 * 
 * This is where the main ECS registry lives, and where other functions and
 * definitions relevant to ECS initialization, configuration, and operation will
 * live.
 */
namespace se::ecs {

    /**
     * Primary Registry.
     *
     * This is where most of the time-critical data is stored. Other forms of
     * data may be stored using other systems.
     */
    extern entt::registry primary_registry;

    /**
     * Initialize the data system.
     */
    void init();

}