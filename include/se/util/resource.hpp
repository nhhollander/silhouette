/** \file
 * Loadble Resource Class Templace.
 * 
 * Copyright 2021 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

#pragma once

#include <vector>
#include <functional>
#include <map>
#include <typeindex>
#include <string>
#include <cstdarg>

/**
 * Resource Utility Namespace.
 *
 * Resources are one of the key components fo the silhouette engine. This
 * namespace contains all of that goodness.
 */
namespace se::util::resource {

    class Resource; // Forward declaration

    /// Resource State
    enum ResourceState {
        NOT_LOADED = 0b00000001,
        LOADING =    0b00000010,
        UNLOADING =  0b00000100,
        LOADED =     0b00001000,
        ERROR =      0b00010000
    };

    /// Stringify `ResourceState`
    const char* resource_state_name(ResourceState state);

    /// Resource state change listener
    typedef std::function<void(Resource*)> StateChangeHandler;

    /// Resource state change listener information structure (internal)
    struct ResourceStateChangeListener {
        /// Callback function
        StateChangeHandler handler;
        /// State Mask
        char mask;
    };

    /**
     * Loadable Resource Virtual Class.
     * 
     * Loadable Resources can be loaded into and out of memory as required by
     * the application. For example, a texture which is not currently in use may
     * be removed from the CPU and/or GPUs memory in order to reduce resource
     * usage.
     */
    class Resource {

        private: 

            /// Resource user counter
            unsigned int resource_user_counter = 0;

            /// Resource State
            ResourceState resource_state =
                resource::ResourceState::NOT_LOADED;

            /// State change listeners
            std::vector<ResourceStateChangeListener> state_change_listeners;

            /// Name of the resource
            std::string name;

            /// Extended name of the resource
            std::string extended_name;

        protected:

            /**
             * Load the resource.
             * 
             * Invoked when the active user counter moves from zero to one.
             */
            virtual void load_() = 0;

            /**
             * Unload the resource.
             * 
             * Invoked when the active user counter moves from one to zero.
             */
            virtual void unload_() = 0;

        public:

            /// Constructor
            Resource();

            /// Destructor
            ~Resource();

            /// Get resource name
            inline std::string get_name() {
                return this->name;
            }

            /// Get extended resource name
            inline std::string get_extended_name() {
                return this->extended_name;
            }

            /// Increment the user counter
            void increment_resource_user_count();

            /// Decrement the user counter
            void decrement_resource_user_count();

            /// Return the user count
            unsigned int get_user_count();

            /* Reload removed because it could result in part of the code
             * attempting to use an unloaded resource. If it's really necessary
             * to have reload functionality, it will need to be implemented
             * differently. */

            /// Get the current state
            inline ResourceState get_resource_state() { return this->resource_state; }

            /*!
             * Change the resource state.
             *
             * Will invoke appropriate state change handlers.
             */
            void set_state(ResourceState state);

            /*!
             * Add a resource state change listener.
             *
             * Listeners will be cleared on invocation.
             */
            void add_state_change_listener(StateChangeHandler func, char trigger_mask = 0xFF);

            /*!
             * Internal configuration function.
             *
             * Used to set read-only name field.
             */
            void __internal_configure(const std::string& name, const std::string& extended_name);

    };

    /**
     * Resource Cache Type.
     */
    typedef std::map<std::string, Resource*> ResourceCache;

    /**
     * Global Resource Cache.
     * 
     * All resources created by the engine are stored in this cache. When a
     * resource is requested, first the engine checks if it is present in this
     * map before instantiating a new one. This prevents resource duplication.
     * 
     * The key of this map is the hash value of the type of the resource class.
     */
    extern std::map<size_t, ResourceCache*> global_cache;

    /**
     * Resource type map.
     * 
     * This map is used for debugging. It associates resource types hashes with
     * the resource name.
     */
    extern std::map<size_t, std::string> type_map;

    /**
     * Retrieve a resource of type.
     * 
     * This method first checks `global_cache` to see if an identical resource
     * has already been created. If found, a pointer to that resource will be
     * returned. If not found, a new instance of the resource will be allocated,
     * stored in the cache, and returned.
     * 
     * Due to template restrictions, this method is implemented in the header
     * instead of `resource.cpp`.
     */
    template<typename T, typename std::enable_if<std::is_base_of<Resource, T>::value>::type* = nullptr, typename... Args>
    T* get(std::string name, Args... args) {
        // Check if a resource of this type has been cached before
        size_t type_hash = typeid(T).hash_code();
        auto global_cache_entry = global_cache.find(typeid(T).hash_code());
        ResourceCache* resource_cache;
        if(global_cache_entry == global_cache.end()) {
            // Create a new map for resources of this type
            resource_cache = new ResourceCache();
            global_cache.insert({
                type_hash,
                resource_cache
            });
            type_map.insert({
                type_hash,
                typeid(T).name()
            });
        } else {
            // Use existing cache
            resource_cache = global_cache_entry->second;
        }
        std::string extended_name = T::calculate_extended_name(name, args...);
        // Check if the resource exists
        auto resource_cache_entry = resource_cache->find(name);
        if(resource_cache_entry == resource_cache->end()) {
            // Allocate and return new type
            T* resource = new T();
            resource->__internal_configure(name, extended_name);
            resource->__configure(args...);
            resource_cache->insert({extended_name, resource});
            return resource;
        } else {
            return dynamic_cast<T*>(resource_cache_entry->second);
        }
    }

    /**
     * Generate resource status report.
     * 
     * Debug method
     */
    void generate_resource_status_report();

}