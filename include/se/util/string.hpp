/** \file
 * String manipulation utilities.
 * 
 * Copyright 2021 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

#pragma once

#include <string>

/**
 * String Utility Namespace.
 * 
 * Various utilities and helpers for dealing with strings.
 */
namespace se::util::string {

    /**
     * Pad a string to the left.
     * 
     * Inserts enough of `pad` at the left side of the string to make it `len`
     * characters long. If the string is longer than `len` and `trim` is `true`,
     * the string will be trimmed to `len`.
     * 
     * This method will not modify `string`, but will return a modified string,
     * unless there is nothing to do, in which case `string` is returned.
     */
    std::string pad_left(std::string string, int len, char pad, bool trim = false);

    /**
     * Pad a string to the right.
     * 
     * Inserts enough of `pad` at the right side of the string to make it `len`
     * characters long. If the string is longer than `len` and `trim` is `true`,
     * the string will be trimmed to `len`.
     * 
     * This method will not modify `string`, but will return a modified string,
     * unless there is nothing to do, in which case `string` is returned.
     */
    std::string pad_right(std::string string, int len, char pad, bool trim = false);

    /**
     * Demangle a class name.
     * 
     * Takes a mangled class name, and returns the demangled form as defined by
     * the implementation. If the given string is not a valid mangled class,
     * or the class for some reason can not be demangled, it will be returned
     * unmodified.
     */
    std::string demangle_class(std::string classname);

}