/** \file
 * Client-side engine configuration.
 * 
 * Copyright 2021 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

#pragma once

#include <string>

/**
 * Configuration Utility Namespace.
 * 
 * This namespace contains relevant definitions to the engine's configuration
 * management system.
 */
namespace se::util::config {

    /*!
     * Initialize the configuration system.
     *
     * Creates the connection between the javascript runtime and the engine
     * that is required in order to pass strings around.
     */
    void init();

    // =========== //
    // = GETTERS = //
    // =========== //

    /// Retrieve a configuration value as a string
    std::string get_str(std::string key, std::string def = "undefined");
    /// Retrieve a configuration value as an int
    int get_int(std::string key, int def = 0);
    /// Retrieve a configuration value as a long
    long get_long(std::string key, long def = 0);
    /// Retrieve a configuration value as a long long
    long long get_long_long(std::string key, long long def = 0);
    /// Retrieve a configuration value as a float
    float get_float(std::string key, float def = 0.0);
    /// Retrieve a configuration value as a double
    double get_double(std::string key, double def = 0.0);
    /// Retrieve a configuration value as a boolean
    bool get_bool(std::string key, bool def = false);

    // =========== //
    // = SETTERS = //
    // =========== //

    /// Set a configuration value to a string
    void set(std::string key, std::string value);
    /// Set a configuration value to a c string
    void set(std::string key, const char* value);
    /// Set a configuration value to an int
    void set(std::string key, int value);
    /// Set a configuration value to a long
    void set(std::string key, long value);
    /// Set a configuration value to a long long
    void set(std::string key, long long value);
    /// Set a configuration value to a float
    void set(std::string key, float value);
    /// Set a configuration value to a double
    void set(std::string key, double value);
    /// Set a configuration value to a boolean
    void set(std::string key, bool value);

    // ========= //
    // = OTHER = //
    // ========= //

    /// Clear a configuration value and use the default
    void unset(std::string key);

}