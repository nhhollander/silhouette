/** \file
 * Hashing Utilities.
 * 
 * Copyright 2020 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

#pragma once

#include <cstdint>
#include <cstdlib>

/**
 * Configuration Utility Namespace.
 *
 * This namespace contains relevant definitions for the hashing system provided
 * by the engine. Note that these hashes are NOT CRYPTOGRAPHICALLY SECURE and
 * really only exist to provide a rudimentary high-speed hashing system for
 * lookups and the distillation of a long unique identifier string into a
 * shorter more mangeable integer. Be aware of the risks for collision if you
 * choose to use these methods.
 */
namespace se::util::hash {

    /**
     * Jenkins' one-at-a-time fast hashing algorithm.
     * 
     * This is a fast hashing function with a collision rate somewhere between
     * FNV-1a and Murmur3. [Here's a pretty good comparision](https://research.neustar.biz/2011/12/29/choosing-a-good-hash-function-part-2/)
     */
    uint32_t jenkins(const void* data, size_t len);

    /**
     * `printf` style wrapper for `se::util::hash::jenkins`.
     *
     * This function will return the jenkins' hash of the given message using
     * standard `printf` formatting.
     */
    uint32_t p_jenkins(const char* fmt, ...);

}