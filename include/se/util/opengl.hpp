/** \file
 * OpenGL Helpers.
 * 
 * Copyright 2021 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

#pragma once

/**
 * OpenGL Utility Namespace.
 *
 * This namespace contains helpful functions and utilities for dealing with
 * various bits of OpenGL weirdness that would be impractical or too complicated
 * to re-implement every time they are needed elsewhere in the engine.
 */
namespace se::util::opengl {

    /*!
     * Clear pending OpenGL errors.
     *
     * Basic information about unaddressed errors will be printed out. Ideally
     * this method will do nothing!
     */
    void clear();

    /*!
     * Check for opengl error.
     *
     * Error information will be printed.
     */
    bool check_error();

    /*!
     * Get the name of an OpenGL error.
     */
    const char* gl_error_name(int error);

    /*!
     * Get the description of an OpenGL error.
     */
    const char* gl_error_desc(int error);

}