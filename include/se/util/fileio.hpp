/** \file
 * Asynchronous file loading system.
 * 
 * Copyright 2020 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

#pragma once

#include <functional>
#include <tuple>
#include <vector>
#include <string>

/**
 * File Input/Output Utility Namespace.
 *
 * This namespace contains relevant definitions to the engine's file management
 * system.
 */
namespace se::util::fileio {

    /**
     * FileIO operation result.
     */
    enum Result {
        SUCCESS,
        FAILURE
    };

    /// Read callback
    typedef std::function<void(std::string,std::vector<char>,Result,void*)> rcallback;

    /// Write callback
    typedef std::function<void(std::string,Result,void*)> wcallback;

    /**
     * Initialize the FileIO system.
     */
    void init();

    /**
     * Put a file in the read queue.
     *
     * Submits a request to read a given file. Once the file has been loaded
     * (or the load has failed), the callback method will be invoked. The
     * callback method should have the following signature:
     *
     * In the event of a server or connection error (5xx) the connection may be
     * retried.
     * 
     * ```
     * void cb(std::string target, std::vector<char> data, se::fileio::Result result);
     * ```
     *
     * @param target   Path to read
     * @param callback Function to run once the read is complete
     * @param retry    Retry load in case of a (5xx) error.
     * @param arg      Optional argument to pass to the callback
     */
    void read(std::string target, se::util::fileio::rcallback callback, bool retry = true, void* arg = nullptr);

    /**
     * File write no-op.
     * 
     * This is a placeholder callback for the file write method. It
     * intentionally has no function.
     */
    void __wrnoop(std::string,Result,void*);

    /**
     * Write a file.
     *
     * Submits a request to write to a given file. Once the file has been
     * written (or the write has failed), the callback method will be invoked.
     * The callback method should have the following signature:
     *
     * ```
     * void cb(std::string target, se::fileio::Result result);
     * ```
     *
     * This method returns the same result codes used in `read()`.
     */
    template <class T> void write(std::string target, T data, se::util::fileio::wcallback callback=__wrnoop, void* arg=nullptr);

    /**
     * Stringify a result.
     * 
     * This debug method returns the stringified name of the result code.
     */
    const char* str_result(Result result);

}