/*! \file
 * Exported functions for debugging from javascript.
 *
 * The functions in this class can be invoked directly from javascript.
 * 
 * Copyright 2021 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

/**
 * Javascript Debugging Bridge Utility Namespace.
 *
 * This namespace contains relevant definitions to the javascript debugging
 * bridge that allows various debugging related actions to be invoked from
 * elements contained within the webpage.
 */
namespace se::util::jsdebug {

    /*!
     * Initialize the jsdebug system.
     */
    void init();

}