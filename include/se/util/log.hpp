/*! \file
 *  Static logging helper library. This basic helper library provides a fast,
 *  configurable, and easy to use logging interface. Other major logging
 *  systems were too complicated, bulky, or would have added more dependencies
 *  to this project, something I have been trying to avoid.
 * 
 *  Copyright 2018 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#pragma once

#include <fmt/core.h>
#include <string>

// Make sure the UTIL_LOG_PATH_OFFSET hack of a workaround is in place
#ifndef UTIL_LOG_PATH_OFFSET
    /*!
     *  Because gcc expands the `__FILE__` macro to include the path as given on
     *  the command line, and cmake uses absolute paths, a lot of unnecessary
     *  information is included. This offset strips out the first n characters
     *  of the absolute path so that only a relative path remains.
     */
    #define UTIL_LOG_PATH_OFFSET 0
    #warning UTIL_LOG_PATH_OFFSET was not set!  See doc/seutil.md for more info.
#endif

/// Logging base base macro for inserting source details
#define __LOG(level, message) \
    se::util::log::log(level, &__FILE__[UTIL_LOG_PATH_OFFSET], __LINE__, __func__, message);
/// Logging base macro for containing exceptions
#define __LOGWRAP(level, message, ...) \
    try { \
        __LOG(level, fmt::format(message, ##__VA_ARGS__)); \
    } catch(const std::runtime_error& __fmt_err__) { \
        __LOG(se::util::log::Level::ERROR, \
            fmt::format("Invalid {} message format string [{}] caused error [{}]", \
                se::util::log::level_to_str(level), message, __fmt_err__.what())); \
    }
/// Log a debug level message
#define DEBUG(message, ...) __LOGWRAP(se::util::log::Level::DEBUG, message, ##__VA_ARGS__)
/// Log an info level message
#define INFO(message, ...) __LOGWRAP(se::util::log::Level::INFO, message, ##__VA_ARGS__)
/// Log a warning level message
#define WARN(message, ...) __LOGWRAP(se::util::log::Level::WARN, message, ##__VA_ARGS__)
/// Log an error level message
#define ERROR(message, ...) __LOGWRAP(se::util::log::Level::ERROR, message, ##__VA_ARGS__)
/// Log a fatal error level message
#define FATAL(message, ...) __LOGWRAP(se::util::log::Level::FATAL, message, ##__VA_ARGS__)

/*!
 * Logging Namespace.
 *
 * This is where the functional components of the logging system live. Generally
 * the functions here will never be called directly, only via expansion of the
 * logging macros.
 */
namespace se::util::log {

    /**
     * Log Level.
     */
    enum Level {
        /**
         * Debugging messages.
         *
         * Messages with this level do not generally contain important runtime
         * information, but can be valuable when debugging the program, or
         * trying to find the source of a problem.
         */
        DEBUG,
        /**
         * Informational messages.
         *
         * Messages with this level contain information relevant to the
         * application's state during runtime, and generally reflect actions
         * performed by or relating to the user.
         */
        INFO,
        /**
         * Warning messages.
         *
         * Messages with this level contain information about potential issues
         * in a game or the engine, although the application should be able to
         * continue operating without major issue
         */
        WARN,
        /**
         * Error messages.
         *
         * Messages with this level contain information about serious issues
         * with the engine or entities in a game, that are most likely
         * noticeable to the user, and could cause application instability.
         */
        ERROR,
        /**
         * Fatal error messages.
         *
         * Messages with this level contain information about extremely serious
         * issues with the engine or application that prevent continued
         * operation of the program. This type of message should only be
         * generated when a failure is so severe the program must terminate.
         */
        FATAL
    };

    /**
     * Message level to string.
     */
    const char* level_to_str(Level level);

    /**
     *  Log a message.
     *
     *  This function formats and logs the message and path information given
     *  via expansion of the above macros.
     *
     *  *Note: This function is intended only to be called via expansion of the
     *  logging macros*
     *
     *  @param level    Log level.
     *  @param fname    Name of source file where the message was generated
     *  @param line     Line number where the message was generated
     *  @param func     Function or method in which the message was generated
     *  @param message  Base logging message and/or `vsnprintf` format string.
     *  @param ...     Variable arguments to be parsed by `vsnprintf`
     */
    void log(Level level, const char* fname, int line, const char* func, std::string message);

    /**
     *  Set the thread name.
     * 
     *  This makes debugging easier, as each message will contain the name of
     *  the thread that it was sent from.
     * 
     *  @param  name    Name of the thread.
     */
    void set_thread_name(const char* name);

}