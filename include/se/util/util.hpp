/** \file
 * This file exists only to document the `se::util` namespace.
 * 
 * All of the actual utility stuff should be placed in specialized files
 * in the same folder as this file.
 * 
 * Copyright 2021 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

/**
 * Utility Namespace.
 *
 * This namespace contains helper utilities used throughout the rest of the
 * engine.
 */
namespace se::util {}
