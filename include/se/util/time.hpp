/** \file
 * Debugging Time Helpers.
 * 
 * Copyright 2021 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

#pragma once

#include <chrono>

/**
 * Time (Debug) Utility Namespace.
 * 
 * When evaluating the performance of parts of the engine, it can be useful to
 * have high resolution timers. This namespace contains the high resolution
 * time points that are used by the `TICK` and `TOCK` macro to evaluate runtime
 * performance with as little impact to actual execution as possible.
 */
namespace se::util::time {
    /// Temporary storage for the current time
    extern std::chrono::high_resolution_clock::time_point now;
    /// Time of the last call to TICK
    extern std::chrono::high_resolution_clock::time_point tick;
    /// Time of the last call to TOCK 
    extern std::chrono::high_resolution_clock::time_point tock;
}

/**
 * Tick!
 * 
 * Calling `TICK` resets the 'tick' and 'tock' timers.
 */
#define TICK \
    se::util::time::now = std::chrono::high_resolution_clock::now(); \
    se::util::time::tick = se::util::time::now; \
    se::util::time::tock = se::util::time::now;

/**
 * Tock!
 * 
 * Calling `TOCK` prints out the value of the 'tick' and 'tock' timers, then
 * resets the 'tock' timer.
 */
#define TOCK(N) \
    { \
        se::util::time::now = std::chrono::high_resolution_clock::now(); \
        std::chrono::duration __time_tick_diff = se::util::time::now - se::util::time::tick; \
        std::chrono::duration __time_tock_diff = se::util::time::now - se::util::time::tock; \
        se::util::time::tock = se::util::time::now; \
        double __time_tick = std::chrono::duration_cast<std::chrono::microseconds>(__time_tick_diff).count() / 1000.0; \
        double __time_tock = std::chrono::duration_cast<std::chrono::microseconds>(__time_tock_diff).count() / 1000.0; \
        DEBUG("TIMING: tick {}ms tock {}ms: {}", __time_tick, __time_tock, N); \
    }
