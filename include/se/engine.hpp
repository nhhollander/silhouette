/** \file
 * This file repsents the core of the Silhouette engine.
 * 
 * Copyright 2021 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 * Licensed under the MIT license (see LICENSE for the complete text)
 */

#pragma once

#include <functional>

/**
 * Engine Namespace.
 *
 * This namespace contains definitions that apply to the engine as a whole,
 * including initialization, de-initialization, run configuration, and
 * asynchronous job management.
 */
namespace se::engine {

    /**
     * Engine initialization method.
     * 
     * Calling this method initializes and launches all subcomponents of the
     * engine.
     */
    void init();

    /**
     * Engine deinitialization method.
     *
     * Calling this method deinitializes and destroys all subcomponents of the
     * engine.
     */
    void deinit();

    /*!
     * Main loop.
     *
     * This function is called at regular intervals by emscripten.
     */
    void loop();

    /*!
     * Engine task.
     *
     * Engine tasks are functions that will be executed one per loop on the
     * main application thread. See `register_job` for more information.
     */
    typedef std::function<void()> Job;

    /*!
     * Enqueue Task.
     *
     * Registers a task to be executed on on the main thread / under the main
     * loop. This task will be executed during the next emscripten main loop
     * invocation.
     */
    void register_job(Job job);

}