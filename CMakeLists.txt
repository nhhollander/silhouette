cmake_minimum_required(VERSION 3.10)
project(silhouette_engine)

SET(CMAKE_EXPORT_COMPILE_COMMANDS "ON")

# Compilation Options
set(SE_DEBUG_OPTIONS "-O2 -g -gsource-map --source-map-base src/")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${SE_DEBUG_OPTIONS} -Wall -fexceptions")
SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${SE_DEBUG_OPTIONS} -s ALLOW_MEMORY_GROWTH=1 -s USE_GLFW=3 -s USE_SDL=2 -s USE_LIBPNG=1 -s EXPORTED_FUNCTIONS=['_main','_se_jsdebug'] -s EXPORTED_RUNTIME_METHODS=['UTF8ToString','cwrap'] -lidbfs.js -s MAX_WEBGL_VERSION=2 -s DEMANGLE_SUPPORT=1")

# Prevent in-source builds
set(CMAKE_DISABLE_SOURCE_CHANGES ON)
set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)
# Set output directories
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ../bin)
# Set the include directory
include_directories(include
    deps/entt/src
    deps/fmt/include
    deps/glm
)
# Use c++ 20
set(CMAKE_CXX_STANDARD 20)

# Dependencies
add_subdirectory(deps/fmt)

# Silhouette library component
add_library(silhouette STATIC
    src/se/ecs/ecs.cpp
    src/se/engine.cpp
    src/se/graphics/frameBuffer.cpp
    src/se/graphics/graphics.cpp
    src/se/graphics/img/img.cpp
    src/se/graphics/img/png.cpp
    src/se/graphics/geometry/loader.cpp
    src/se/graphics/geometry/obj.cpp
    src/se/graphics/geometry.cpp
    src/se/graphics/graphics.cpp
    src/se/graphics/program.cpp
    src/se/graphics/shader.cpp
    src/se/graphics/texture.cpp
    src/se/util/config.cpp
    src/se/util/fileio.cpp
    src/se/util/hash.cpp
    src/se/util/jsdebug.cpp
    src/se/util/opengl.cpp
    src/se/util/resource.cpp
    src/se/util/log.cpp
    src/se/util/string.cpp
    src/se/util/time.cpp
)
# Link to required libraries
target_link_libraries(silhouette
    pthread
    GL
    ${SDL2_LIBRARIES}
    ${GLEW_LIBRARIES}
    ${PNG_LIBRARIES}
    fmt::fmt
)
# Calculate the log text offset (workaround for absolute paths in cmake)
string(LENGTH ${CMAKE_SOURCE_DIR}/src/ UTIL_LOG_PATH_OFFSET)
target_compile_options(silhouette PRIVATE "-DUTIL_LOG_PATH_OFFSET=${UTIL_LOG_PATH_OFFSET}")

# Build examples
add_subdirectory(examples/test)
