[root](/README.md)/docs

# Documentation

Welcome to the silhouette engine documentation. Here you will find automatically and manually generated documentation for the various components of the engine. AT some point in the future I will probably get around to filling out this folder with more useful documetnation. An overview of the documentation is available below.

## Contents

 * [building](building.md)
 * [overview](overview.md)
 * [generated documentation](generated/html/index.html)
