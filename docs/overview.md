[root](/README.md)/[docs](/docs/DOCS.md)/overview

# Silhouette Engine

The silhouette engine is a product of many years of messing around, and was
designed primarily as an educational product, which I hoped would maybe at some
point become a usable game engine. Below is a general overview of the structure
of the engine.

## Overview

Note that this diagram is a work in progress, and is subject to change as the
engine evolves.

![](./img/layout_and_overview.drawio.svg)
