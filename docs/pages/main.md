# Welcome to the Silhouette Engine Documentation

Silhouette is a basic 3D game engine for web browsers written in C++ and powered
by emscripten.

An in-depth [technical overview](pages/overview.md) is available.

## Building

Building the engine is handled via gnu `make`. Running `make` without any
arguments will display an overview of available targets.

## License

[MIT](pages/license.md)