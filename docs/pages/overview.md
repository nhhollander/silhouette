# Technical Overview

[root](index.html)/overview.md

Unlike many other game engines, silhouette is designed specifically to be run
in a web browser. There are many intricacies when developing for the web that
are usually not taken into account in the designs of other engines. The
restrictions imposed and features provided by the browser are responsible for
many of the decisions described here.

## Table of Contents

* [Overview](#overview)
* [Graphics System](#graphics-system)
  * [Renderer](#graphics-system-resources)
  * [Resources](#graphics-system-resources)
* [Logic System](#logic-system)
* [Input System](#input-system)
* [Utilities](#utilities)
  * [Resource Management](#utilities-resource-management)
  * [Logging](#utilities-logging)

<a id="overview"></a>
## Overview

The below diagram provides an overview of the engine layout and core components.

![Silhouette engine layout and overview](img/layout_and_overview.drawio.svg)

<a id="graphics-system"></a>
## Graphics System

The graphics system contains all the code and modules which are responsible for
stuff that gets drawn to the screen. The graphics system, as well as the engine
at large, utilizes [Entity Component
System](https://en.wikipedia.org/wiki/Entity_component_system) to define
`Renderable` objects.

<a id="graphics-system-renderer"></a>
### Renderer

Silhouette implements a rather basic and direct render system. It is
significantly less feature-rich and versatile compared to the render systems
found in engines like Godot or UE4, but capable of more efficient operation.



<a id="graphics-system-resources"></a>
### Resources

In order to facilitate rendering, a few resources are predefined by the engine.
For more information about the resource loading system, see [Resource
Management](#utilities-resource-management). Resources are efficiently cached,
such that creating or loading 1000 identical Textures will produce 1000 pointers
to the same Texture object.

#### Geometry
Defined in the `se::graphics::geometry` namespace.

Geometry is a representation of a renderable mesh, usually loaded from an OBJ
file or a manually specified set of points.

```{.cpp}
using namespace se::util;
using namespace se::graphics::geometry;
Geometry* geom = resource::get<Geometry>("model_name.obj");
```

#### Program
Defined in the `se::graphics::program` namespace.

OpenGL programs are the result of linking a number of shaders together into a
single object which can be activated by a single api call. Creation of a program
will automatically create the associated `se::graphics::shader::Shader` objects.

```{.cpp}
using namespace se::util;
using namespaec se::graphics::program;
Program* prog = resource::get<Program>("program_name");
```

#### Shader
Defined in the `se::graphics::shader` namespace.

OpenGL shaders represent a single operation in the render process, for the
silhouette engine this is either a vertex shader or a fragment shader. Under
normal circumstances, there is no need to manually get shader objects, as this
should be handled by `Program`.

```{.cpp}
using namespace se::util;
using namespace se::graphics::shader;
Shader* vert = resource::get<Shader>("program_name", GL_VERTEX_SHADER);
Shader* frag = resource::get<Shader>("program_name", GL_FRAGMENT_SHADER);
```

#### Texture
Defined in the `se::graphics::texture` namespace.

Textures represent a set of pixels bound to the graphics device. There are a
variety of supported texture formats. Due to limitations of WebGL 2, only some
texture formats are renderable, which is required to bind them to a Framebuffer
for deferred rendering.

Information about the renderability and storage types for various GL formats can
be found at the [MDN Web
Docs](https://developer.mozilla.org/en-US/docs/Web/API/WebGLRenderingContext/texImage2D)
or the [WebGL 2
Specification](https://www.khronos.org/registry/webgl/specs/latest/2.0/#3.7.6)

*Note to developers: Please keep this list updated*

| Texture  | GL Format             | Renderable |
|----------|-----------------------|------------|
| `GRAY8`  | `R8`                  | Yes        |
| `GRAY16` | `R16UI`               | Yes        |
| `RGB8`   | `RGB8`                | Yes        |
| `RGB16`  | `RGB16UI`             | No         |
| `RGBA8`  | `RGBA8`               | Yes        |
| `RGBA16` | `RGBA16UI`            | Yes        |
| `RGBF`   | `RGB32F`              | No         |
| `RGBAF`  | `RGBA32F`             | Yes<sup>†</sup>|
| `DEPTH`  | `DEPTH_COMPONENT_32F` | N/A        |

<sup>†</sup> Requires the
[`EXT_color_buffer_float`]
(https://developer.mozilla.org/en-US/docs/Web/API/EXT_color_buffer_float)
extension.

#### FrameBuffer

Framebuffers are renderable targets used in the graphics system to allow render
data to be written to an offscreen buffer and processed before final output.
Currently Framebuffers require that the client has the
[`EXT_color_buffer_float`]
(https://developer.mozilla.org/en-US/docs/Web/API/EXT_color_buffer_float)
extension in order to render calculated position information to a `RGBAF`
texture. Position information *must* be stored in this format because it
supports both high resolution *and* decimal places.

<a id="logic-system"></a>
## Logic System

*Not yet Implemented*

<a id="input-system"></a>
## Input System

*Not yet Implemented*

<a id="utilities"></a>
## Utilities

The utility system provides various general purpose components that are used
throughout the rest of the engine.

<a id="utilities-resource-management"></a>
### Resource Management

The resource manager is responsible for overseeing the retrieval, loading, and
unloading of other engine components. A resource inherits from the
`se::util::LoadableResource` class and overrides several important methods. Most
internal logic, including loading and unloading, is performed by the parent
class.

In order to manage what data is loaded at any given time, the resource base
class provides two *very* important methods: `increment_resource_user_count` and
`decrement_resource_user_count`. These do *not* represent the number of handles
to an object, as handles are unregulated and remain valid for the lifetime of
the engine. Instead the user count represents the number of active objects in
the engine that are performing logic, rendering, or other operations using the
resource at a time. When this value increases from 0 to 1, the resource will
load its data into memory and make it available, and when the value decreases
back to zero, the object releases as much of its memory as possible.

While this approach does result in a small amount of additional memory being
consumed for each object used at any point during a single run of the engine,
the performance and simplicity far outweighs this. Technically it's not a memory
leak because a handle to the data is not lost, but it is not entirely
unallocated by design.

Below is a sample resource. For simplicity the code and headers are combined,
although it is recommended that they be separated in production code.

```{.cpp}
#include <se/util/loadableResource.hpp>

using namespace se::util;

class Example : public LoadableResource {

    public:

        /**
         * Configuration Method.
         *
         * This method is called after object construction for new instances
         * with user-specified arguments. You can have multiple configuration
         * methods with different signatures, each of which will be available
         * when getting this object.
         */
        void __configure(int number) {
            this->special_number = number;
        }

        /**
         * Calculate the extended name for this resource.
         *
         * The extended name must be unique to every instance of this class and
         * every combination of configuration options. Like `__configure`, there
         * can be multiple extended name configuration methods, each of which
         * has a different signature.
         */
        static std::string calculate_extended_name(std::string name, int num) {
            // Uses the fmt.dev library for simplicity.
            return fmt::format("{}:{}", name, int);
        }

    private:

        /**
         * Loadable Data.
         *
         * This is where the data that makes up a resource is stored. When
         * the resource is created it will be empty, but when the resource
         * enters the `LOADED` state then it should be populated with valid
         * data.
         */
        std::string data;

        /**
         * Name of the resource.
         *
         * This is a unique identifier for the resource which is used for
         * loading from a remote server as well as identifying the resource
         * in logs.
         */
        std::string name;

        /**
         * Special Number.
         *
         * This value is used to demonstrate the configuration method.
         */
        int special_number;

        /**
         * Load method.
         *
         * This method will be invoked by the resource manager when the 
         * resource is needed.
         */
        void load_() override {
            this->set_state(Loadableresource::ResourceState::LOADING);
            /* Perform the loading operation here. This will usually involve
             * sending a web request using the fileio system. This function
             * can return before loading is complete, and generally will.
             * Normally the state at the end of the function will be
             * `LOADING`, `LOADED`, or `ERROR`.
             */
            this->set_state(Loadableresource::ResourceState::LOADED);
        }

        /**
         * Unload method.
         *
         * This method will be invoked by the resource manager when the
         * resource is no longer needed.
         */
        void unload_() override {
            this->set_state(Loadableresource::ResourceState::UNLOADING);
            /* Perform the unloading operation here. This should generally be
             * a fairly quick operation, and the state at the end of the
             * function should be set to `NOT_LOADED`.
             */
            this->set_state(Loadableresource::ResourceState::NOT_LOADED);
        }
}
```

To get an instance of a resource, use the `get` template method. To get an
instance of the above demo resource, you would use the following.

```{.cpp}
using namespace se::util;
Example* example = resource::get<Example>("example_resource", 42);
example->get_name(); // Returns "example_resource"
example->get_extended_name(); // Returns "example_resource:42"

```

The pointer to an object returned by `get` is always valid and must *never* be
`delete`d.


<a id="utilities-logging"></a>
### Logging

Silhouette implements a basic logging system. Messages are displayed in the
browser console. ANSI escape sequences are supported. Log message can contain
parameters, which are stringified using the [fmt.dev](https://fmt.dev/)
formatting library. Messages should generally follow the following guidelines:

* `DEBUG`: Used for testing and development.
* `INFO`: Describes engine operations and generally non-repetitive tasks.
* `WARN`: Abnormal but recoverable events.
* `ERROR`: Discrete task failure, may not be recoverable.
* `FATAL`: Complete engine failure, program can not continue to run.

```{.cpp}
#include <se/util/log.hpp>

// ...

DEBUG("This is a debug message");
INFO("This is an information message");
WARN("This is a warning message");
ERROR("This is an error message");
FATAL("This is a fatal message!");

int foo = 23;
INFO("The super secret number is {}", foo);

std::string bar = "silhouette";
WARN("{} is the best game engine!", bar);
```
