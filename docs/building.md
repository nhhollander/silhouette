[root](/README.md)/[docs](/docs/DOCS.md)/building

# Buildilng

Currently the engine is only known to be buildable for Linux and WebAssembly,
although it should be possible to compile it for other platforms.

## Dependencies

Silhouette depends on the following utilities:

* `TODO: Create a list of dependencies`

## Building

`TODO: Include instructions on enabling or disabling examples`

### Linux

Navigate a command window to the root of the project and execute the following
commands. Optionally you can replace `$(nproc)` with the number of threads you
would like to use for building.

```bash
$ cd build
$ make -j$(nproc)
```