SHELL := /bin/bash

ENVSETUP := $(shell bash -c "mkdir -p build; source deps/emsdk/emsdk_env.sh 2> build/emsdk_env.log; env | sed 's/=/:=/' | sed 's/^/export /' > build/makeenv")
include build/makeenv

.PHONY: help
help: ## Display help and exit
	@printf "\033[1;35mSilhouette Engine Build System\033[0m\n"
	@grep -E '^[a-zA-Z_]+:.*?##.*$$' Makefile | sort | awk 'BEGIN {FS = "(: ?| ?## ?)"}; { printf " \033[33m%s:\033[0m\n  %s\n", $$1, $$3; }'

all: build docs

.repo_initialized:
	@printf "\033[33mInitializing workspace...\033[0m\n"
	@./scripts/init.sh

.PHONY: build
build: .repo_initialized ## Update the CMake Configuration, build the project, and execute post-build steps.
	@printf "\033[33mConfiguring wasm...\033[0m\n"
	@cd build; emcmake cmake -DCMAKE_BUILD_TYPE=Debug ../
	@printf "\033[33mBuilding wasm...\033[0m\n"
	@cd build; make -j$(nproc) --no-print-directory
	@printf "\033[33mGenerating data symlinks...\033[0m\n"
	@./scripts/generate_wasm_symlinks.sh
	@printf "\033[33mRemapping source maps...\033[0m\n"
	@find -name "*.wasm.map" -exec ./scripts/remap_sourcemap.py {} \;

docsdeps = $(shell find src/) $(shell find include/) $(shell find docs/ -regextype egrep -regex ".*/.*\.(md|svg)")
.PHONY: docs $(docsdeps)
docs: docs/generated/html/index.html
docs/generated/html/index.html: $(docsdeps) ## Generate documentation
	@./scripts/generate_docs.sh

clean: ## Remove all generated build and documentation files
	@printf "\033[33mDeleting build files...\033[0m\n"
	@rm -rdf build/*
	@printf "\033[33mDeleting generated documentation...\033[0m\n"
	@rm -rdf docs/generated/*

run: build ## Launch the program
	@printf "\033[33mRunning WASM...\033[0m\n"
	@./scripts/run.sh

debug: all
	@printf "\033[33mDebugging Project...\033[0m\n"
	@printf "\033[31mNot yet implemented\033[0m\n"

rebuild: clean all ## Clean the project and then build
	@printf "\033[33mRebuilt Project\033[0m\n"

cloc: ## Count the number of lines of code in the application
	@printf "\033[33mCounting lines of code\033[0m\n"
	@./scripts/count_lines.sh
