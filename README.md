root/

# Silhouette Engine a5

Silhouette is a basic 3D engine written in OpenGL. The goal of this project is to create a reasonably fast 3D rendering system which is available across a wide variety of platforms, including the web (via WebAssembly and WebGL).

I started work on Silhouette in the summer of 2017 with a series of experiments in Vulkan and OpenGL, from which I leared a considerable amount of my knowledge about 3D graphics. Since the original inception of this project, it has been cut down and re-built from the ground up several times, generally as a result of fundamental issues that could not be solved without re-writing a majority of the codebase.

Currently silhouette is on rewrite number 5, hence version a5.

## Dependencies

TODO: Complete this section

## Building

See [docs/building](docs/building.md)

## Examples and Reference Implementations

See [examples](examples/EXAMPLES.md)

## Documentation

See [docs](docs/DOCS.md)

## License

This project is licensed under the [MIT license](LICENSE)
