#include "se/engine.hpp"
#include "se/util/log.hpp"
#include "se/graphics/graphics.hpp"
#include "se/graphics/program.hpp"
#include "se/graphics/texture.hpp"
#include "se/graphics/geometry.hpp"
#include "se/graphics/frameBuffer.hpp"
#include "se/util/fileio.hpp"
#include "se/util/config.hpp"

#include <chrono>
#include <thread>
#include <vector>
#include <emscripten.h>

using namespace se::graphics::texture;
using namespace se::graphics::geometry;
using namespace se::graphics::program;
using namespace se::graphics::framebuffer;
using namespace se::util;

void callback(std::string target, std::vector<char> data, fileio::Result result, void* arg) {
    DEBUG("File request result for [{}] was [{}]",
        target, fileio::str_result(result));
    std::string str(data.begin(), data.end());
    DEBUG("Contents: [{}]", str);
}

void timeout_callback(std::string target, std::vector<char> data, fileio::Result result, void* arg) {
    DEBUG("Timeout Test result for [{}] was [{}]", target, fileio::str_result(result));
    std::string str(data.begin(), data.end());
    DEBUG("Contents: [{}]", str);
}

int main() {
    se::engine::init();

    DEBUG("[{}] Invalid debug message!");

    Program* tp = resource::get<Program>("test");
    tp->increment_resource_user_count();

    fileio::read("test_file.txt", callback);

    INFO("Loading test textures!");
    Texture* t1 = resource::get<Texture>("test_gray8.png");
    Texture* t2 = resource::get<Texture>("test_gray16.png");
    Texture* t3 = resource::get<Texture>("test_rgb8.png");
    Texture* t4 = resource::get<Texture>("test_rgb16.png");
    Texture* t5 = resource::get<Texture>("test_rgba8.png");
    Texture* t6 = resource::get<Texture>("test_rgba16.png");
    Texture* err = resource::get<Texture>("error_texture.png");
    Texture* virt = resource::get<Texture>("virtual_test_texture", 256, 256, PixelFormat::RGBA16);
    Geometry* geom = resource::get<Geometry>("suzanne.obj");
    INFO("Incrementing user counts");
    t1->increment_resource_user_count();
    t2->increment_resource_user_count();
    t3->increment_resource_user_count();
    t4->increment_resource_user_count();
    t5->increment_resource_user_count();
    t6->increment_resource_user_count();
    err->increment_resource_user_count();
    virt->increment_resource_user_count();
    geom->increment_resource_user_count();
    // INFO("Decrementing user counts");
    // t1->decrement_resource_user_count();
    // t2->decrement_resource_user_count();
    // t3->decrement_resource_user_count();
    // t4->decrement_resource_user_count();
    // t5->decrement_resource_user_count();
    // t6->decrement_resource_user_count();
    // err->decrement_resource_user_count();
    // virt->decrement_resource_user_count();
    // geom->decrement_resource_user_count();

    resource::generate_resource_status_report();

    //std::string test = se::util::config::get_str("test_key", "default_value");
    //INFO("Test Value: {}", test);

    //se::util::config::set("test_key", "guacamole");
    //double double_test = se::util::config::get_double("test_key", 1234.5);
    //INFO("Test Value: {}", double_test);

    //INFO("==ENDING IN 2 SECOND==");
    //std::this_thread::sleep_for(std::chrono::seconds(2));
    //INFO("==ENDING==");

    //se::util::generate_resource_status_report();

    //se::engine::deinit();
}