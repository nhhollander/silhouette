[root](/README.md)/examples

# Examples

In this folder you can find some examples of how to use the silhouette engine.

 * [Testing Implementation](test/README.md)

Instructions for buildling the examples can be found in
[docs/building](/docs/building.md)